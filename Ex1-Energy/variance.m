% load data 
load("results\results1.txt");
width_of_tray = 0.2; 

heights = results1(:, 1);

% fix height with width of tray 
heights = heights + width_of_tray; 
distances = results1(:, 2);

unique_heights = unique(heights);

dots_results = [];

for i = 1:length(unique_heights) 
    curr_height = unique_heights(i);
    bitmap = heights == curr_height;
    dists = distances(bitmap); 
    
    % Expected value is mean, because the probability graph is flat 
    E = mean(dists);
    amount = length(dists);    
    
    my_variance = sum((dists - E) .^ 2)/(amount-1);
    % the "1" parameter means we divide by amount and not (amount - 1)
    % according to doc.
    mat_var = var(dists, 1);
    
    disp("Height: " + curr_height + " variance: " + my_variance) % + " mat_var: " + mat_var);
end
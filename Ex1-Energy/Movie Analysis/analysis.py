import math


class Point(object):
	def __init__(self, x, y):
		self.x = x
		self.y = y
	
	def dist(self, p2):
		return math.sqrt((p2.x-self.x)**2 + (p2.y-self.y)**2)


def dist(p1, p2):
	return p1.dist(p2)


FPS = 170


def calc_velocity(p1_on_ruler, p2_on_ruler, ball_point_a, ball_point_b, frames_amount):	
	cm_to_pixel = dist(p1_on_ruler, p2_on_ruler)
	pixel_to_cm = 1/cm_to_pixel

	dx = dist(ball_point_a, ball_point_b)
	d_cm = dx * pixel_to_cm
	dt = frames_amount / FPS
	v = d_cm / dt
	return v

# Video4
# Calc A
p1_on_ruler = Point(148, 209)
p2_on_ruler = Point(146, 171)
ball_point_a = Point(814, 621)
ball_point_b = Point(842, 621)
v1_a = calc_velocity(p1_on_ruler, p2_on_ruler, ball_point_a, ball_point_b, 1)

# Calc B
p1_on_ruler = Point(148, 209)
p2_on_ruler = Point(146, 171)
ball_point_a = Point(813, 619)
ball_point_b = Point(931, 621)
v1_b = calc_velocity(p1_on_ruler, p2_on_ruler, ball_point_a, ball_point_b, 5)


# Video 6
p1_on_ruler = Point(148, 209)
p2_on_ruler = Point(146, 171)
ball_point_a = Point(796, 620)
ball_point_b = Point(883, 620)
v2 = calc_velocity(p1_on_ruler, p2_on_ruler, ball_point_a, ball_point_b, 4)

# Video 5
p1_on_ruler = Point(148, 209)
p2_on_ruler = Point(146, 171)
ball_point_a = Point(721, 618)
ball_point_b = Point(918, 621)
v3 = calc_velocity(p1_on_ruler, p2_on_ruler, ball_point_a, ball_point_b, 8)

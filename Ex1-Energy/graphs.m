close all;

% load data 
load("results\results1.txt");
width_of_tray = 0.2; 

heights = results1(:, 1);

% fix height with width of tray 
heights = heights + width_of_tray; 
distances = results1(:, 2);

% Find mean distances
unique_heights = unique(heights);
mean_dists = zeros(length(unique_heights), 1);

for i = 1:length(unique_heights) 
    curr_height = unique_heights(i);
    bitmap = heights == curr_height;
    dists = distances(bitmap); 
    mean_dists(i) = mean(dists);
end

y_err = zeros(length(unique_heights), 1);
% find height errors 
for i = 1:length(unique_heights) 
    curr_height = unique_heights(i);
    bitmap = heights == curr_height;
    dists = distances(bitmap);
    y_err(i) = std(dists) / sqrt(length(dists));    
end

% Fit: 'Distance over Height'.
[xData, yData] = prepareCurveData( unique_heights, mean_dists );

% Set up fittype and options.
ft = fittype( 'power1' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
a = fitresult.a;
b = fitresult.b;
l = linspace(0,30, 300);

% Fit error bounds:
confidence_bounds = confint(fitresult);
fit_errs = (confidence_bounds(2,:) - confidence_bounds(1,:)) / 4;
a_err = fit_errs(1);
b_err = fit_errs(2);

% error from location of ruler, and accuracy of measurement
x_err = 0.1*ones(length(mean_dists),1);

figure( 'Name', 'Distance over Height' );
e = errorbar(unique_heights, mean_dists, y_err, y_err, x_err, x_err, '.r');
% e.CapSize = 0;
% e.LineWidth = 0.1;
hold on; 
h = plot(l, a*l.^b);

title('Distance over Height');
axis([0, 30, 0, 30]);
xlabel("Height H (cm)");
ylabel("Distance (cm)");
set(gca, 'FontSize', 18);
% set(gca, 'XScale', 'log', 'YScale', 'log');

figure;
% residuals graph
residuals = (mean_dists - a*unique_heights.^b) ./ y_err;
bar(unique_heights, residuals);
title('Normalized Difference from Theoretical Results');
xlabel("Height H (cm)");
ylabel("Normalized Difference");
set(gca, 'FontSize', 18);

% Calc c parameter:
ft = fittype( 'power2' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
confidence_bounds = confint(fitresult);
c_bounds = confidence_bounds(:,3);
c = fitresult.c;
% Divide by 4 to get 1 std bound
c_err = (c_bounds(2) - c_bounds(1)) / 4;

% Calc v by reverse ballistic
v = mean_dists.*sqrt(g./(2.*unique_heights));
plot(unique_heights, v, '*r');
title('Expected Velocity vs Height');
xlabel("Height H (cm)");
ylabel("Expected Velocity (cm/s)");
set(gca, 'FontSize', 18);

% Calc v error:
v1 = 105.44816;
v2 = 97.16814;
v3 = 110.02496;
v_err 
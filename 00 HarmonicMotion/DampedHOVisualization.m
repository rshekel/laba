%Damped harmonic oscillator demonstration
%Solve the differential equation d2x/dt^2+alpha/m*dx/dt + k/m*x = 0
%Written by: Nadav Katz
%First written: 29/11/2017
%Method: use a superposition of two oscillating/damped solutions, based on
%the exponential/complex solution representation.

close all;
clear all;

%define mass m, spring constant k and damping parameter alpha
m=1;
k=1;
alpha=0.05;

%If greater than 1 - strong damping
DampingParam=alpha^2/(4*m^2)/(k/m)

%initial conditions
x0=0;
v0=4;

%define time vector
t=0:0.001:10;

%solve using the ansatz x(t)=a*exp(-g*t)
%leads to the following two solution rates g1 and g2
g1=alpha/2/m+sqrt(alpha^2/4/m^2-k/m);
g2=alpha/2/m-sqrt(alpha^2/4/m^2-k/m);

%solve for amplitudes a and b in full solution: x=a*exp(-g1*t)+b*exp(-g2*t)
a=(v0+g2*x0)/(g2-g1);
b=(v0+g1*x0)/(g1-g2);

%calculate x and v for the full vector t
x=a*exp(-g1*t)+b*exp(-g2*t);
v=-g1*a*exp(-g1*t)-g2*b*exp(-g2*t);

%plot x(t) and v(t) results vs. time
subplot(1,2,1);
plot(t,x,t,v);
xlabel('time');
ylabel('x(t) and v(t)');
title ('x(t) and v(t)');
legend('x(t)','v(t)');

%plot parametric plot
subplot(1,2,2);
plot(x,v);
xlabel('x');
ylabel('v');
title('parametric plot');
axis equal;

%plot energy (kinetic/potential/total) vs. time
figure;
plot(t,0.5*m*v.^2,t,0.5*k*x.^2,t,0.5*m*v.^2+0.5*k*x.^2);
xlabel('time');
ylabel('Energy');
legend('kinetic energy','potential energy','total energy');
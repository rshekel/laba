%Damped-driven harmonic oscillator demonstration
%Solve the differential equation 
%d2x/dt^2+alpha/m*dx/dt + k/m*x = F0/m*cos(we*t)
%Written by: Nadav Katz
%First written: 5/12/2017
%Method: use a superposition of two oscillating/damped solutions, based on
%the exponential/complex solution representation and add the steady state
%solution.
close all;
clear all;

%define mass m, spring constant k and damping parameter alpha
m=1;
k=1;
%use 0<alpha<2 for oscillating solutions,
%note that alpha=2 (critical damping) isn't treated correctly here.
%Also alpha=0 is divergent.
%Both these values can be approximated arbitrarily well however, by the
%code here.
alpha=0.01;

%If greater than 1 - strong damping
DampingParam=alpha^2/(4*m^2)/(k/m)

%Drive parameters, assuming a drive F0*cos(we*t)
F0=1;
we=1;

%initial conditions
x0=4;
v0=0;

%define time vector
t=0:0.01:100;

%solve using the ansatz x(t)=a*exp(-g*t) for the transient part
%leads to the following two solution rates g1 and g2
g1=alpha/2/m+sqrt(alpha^2/4/m^2-k/m);
g2=alpha/2/m-sqrt(alpha^2/4/m^2-k/m);

%steady state solution
Ad=F0/m*1./sqrt((k/m-we^2).^2+alpha^2*we^2/m^2);
phiD=atan2(-alpha/m*we,k/m-we^2);

%determine transient constants a and b
x0t=x0-Ad*cos(phiD);
v0t=v0+we*Ad*sin(phiD);

%solve for amplitudes a and b in full solution: x=a*exp(-g1*t)+b*exp(-g2*t)
a=(v0t+g2*x0t)/(g2-g1);
b=(v0t+g1*x0t)/(g1-g2);

%general solution
x_damped = a*exp(-g1*t)+b*exp(-g2*t);
x_driven = Ad*cos(we*t+phiD);
x = x_damped + x_driven;
v=-g1*a*exp(-g1*t)-g2*b*exp(-g2*t)-Ad*we*sin(we*t+phiD);

%plot x(t) and v(t) results vs. time
subplot(1,2,1);
plot(t,x,t,v);
xlabel('time');
ylabel('x(t) and v(t)');
title ('x(t) and v(t)');
legend('x(t)','v(t)');

%plot parametric plot
subplot(1,2,2);
plot(x,v);
xlabel('x');
ylabel('v');
title('parametric plot');
axis equal;

%plot separately damped motion, driven motion, and total motion 
figure;
plot(t,x_damped,t,x_driven, t, x);
xlabel('time');
ylabel('x(t)');
title ('damped, driven, and total x motion');
legend('damped x motion','driven x motion', 'total x motion');

%plot energy (kinetic/potential/total) vs. time
figure;
plot(t,0.5*m*v.^2,t,0.5*k*x.^2,t,0.5*m*v.^2+0.5*k*x.^2);
xlabel('time');
ylabel('Energy');
legend('kinetic energy','potential energy','total energy');
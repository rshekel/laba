[C, L] = meshgrid(-20e-3:0.03e-3:-10e-3, 0.1e-9:0.03e-9:5e-9);
[n_sigma1, n_sigma2, n_sigma3, n_sigma_tot] = calc_error_for_guess(C, L);
[min_val, min_index] = min(n_sigma_tot(:));

disp("Min error: " + min_val + " at C: " + C(min_index) + " L: " + L(min_index));

mesh(C, L, n_sigma_tot);
title("n_\sigma / parasite C,L");
xlabel('C');
ylabel('L');

function fitresult = fit_to_exp(path, ignore_above, ignore_below)
if nargin == 1
    ignore_above = 0.9;
    ignore_below = 0;
end

%% Import data
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

%% Cut unimportant first part
max_v = max(v);
below_indexes = find(v < ignore_above*max_v);
start_index = below_indexes(1);
above_indexes = find(v > ignore_below*max_v);
end_index = above_indexes(end);
t = t(start_index:end_index); 
v = v(start_index:end_index);

%% Fit to exp
[xData, yData] = prepareCurveData( t, v );

% Set up fittype and options.
ft = fittype( 'a*exp(-b*x)+c', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [2 56000 0];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

%{
%% Plot
figure;
plot(fitresult, xData, yData);
title(path);
%}
end
V_over_I = load("..\results\rx_ohm.txt");
V_over_I_reversed_polar = load("..\results\rx_ohm_reversed_polarization.txt");

% V_over_I = V_over_I_reversed_polar;

Vs = V_over_I(:,1);
Is = V_over_I(:,2);
Is = Is ./ 1000;  % Measurements are in mili Ampere

%% Fit
[xData, yData] = prepareCurveData( Is, Vs );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
get_params_and_errors(fitresult);

%% Plot
figure;

% Using 0.5% error
x_err = 0.005.*Is;
y_err = 0.005.*Vs;
errorbar(Is, Vs, y_err, y_err, x_err, x_err, '.');
hold on;

% Plot fit with data.
h = plot( fitresult );
l = legend();
set(l, 'visible', 'off');
% Label axes
title("Voltage (V) vs. Current (A)");
xlabel("Current (A)");
ylabel("Voltage (V)");
grid on

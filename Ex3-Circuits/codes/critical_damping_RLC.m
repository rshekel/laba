close all;
RESULTS_FOLDER = "../results/RLC/CriticalDamping/";

%% Plot critical damping:
path = RESULTS_FOLDER + "critical_dampening1_7.00K.csv";
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

figure;
plot(t, v, '.', "MarkerSize", 2);
title("Voltage on Capacitor - Critical Damping - RLC Circuit");
annotation("textbox", [.15, .1, .2, .2], "String", "R=7K\Omega", 'FitBoxToText','on', "FontSize", 19);
xlabel("Time (s)")
ylabel("Voltage (V)");
grid on;
set(gca, "FontSize", 15);

%% Critical damping - fit
path = RESULTS_FOLDER + "critical_dampening1_7.00K.csv";
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

start_t = 9e-7;
start_index = find(abs(t-start_t) <= (100000*eps), 1);
end_t = 2.5e-5;
end_index = find(abs(t-end_t) <= (100000*eps), 1);
t = t(start_index:end_index);
v = v(start_index:end_index);

% Fit to damped sin:
[xData, yData] = prepareCurveData( t, v );

% Set up fittype and options.
ft = fittype( '(a+c*t)*(exp(-b*t)) + e', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [6 2e5 3e6 0];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot
figure;
plot(t, v, '.', "MarkerSize", 2);
hold on;
plot(fitresult);
legend("off");
title("Voltage on Capacitor - Critical Damping - RLC Circuit");
annotation("textbox", [.15, .1, .2, .2], "String", "R=7K\Omega", 'FitBoxToText','on', "FontSize", 19);
xlabel("Time (s)")
ylabel("Voltage (V)");
grid on;
set(gca, "FontSize", 15);

%% Plot over damping:
path = RESULTS_FOLDER + "over_dampening_8.43K.csv";
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

figure;
plot(t, v, '.', "MarkerSize", 2);
title("Voltage on Capacitor - Over Damping - RLC Circuit");
annotation("textbox", [.3, .1, .2, .2], "String", "R=8.43K\Omega", 'FitBoxToText','on', "FontSize", 19);
grid on;
set(gca, "FontSize", 15);

% Fit to over damping
start_t = -8.4e-07;
start_index = find(abs(t-start_t) <= (100000*eps), 1);
t = t(start_index:end);
v = v(start_index:end);

% Fit to damped sin:
[xData, yData] = prepareCurveData( t, v );

% Set up fittype and options.
ft = fittype( 'a*(exp(-b*t)) + c*(exp(-d*t))+ e', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [-Inf -Inf -Inf -Inf -10];
opts.StartPoint = [-30 250000 30 200000 0];
opts.Upper = [Inf Inf Inf Inf 10];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

hold on;
original_ylim = ylim;
plot(fitresult);
ylim(original_ylim);
legend("V", "Fit to a*e^{-b*t} + c*e^{-d*t}+ f");
xlabel("Time (s)")
ylabel("Voltage (V)");

disp("Fit params for over damped");
get_params_and_errors(fitresult);

%% Plot under damping - 1:
path = RESULTS_FOLDER + "low_dampening_750Ohm.csv";
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

figure;
plot(t, v, '.', "MarkerSize", 2);
title("Voltage on Capacitor - Under Damping - RLC Circuit");
annotation("textbox", [.6, .1, .2, .2], "String", "R=750\Omega", 'FitBoxToText','on', "FontSize", 19);
xlabel("Time (s)")
ylabel("Voltage (V)");
grid on;
set(gca, "FontSize", 15);

%% Plot under damping - 2:
path = RESULTS_FOLDER + "low_dampening_11.6Ohm.csv";
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

figure;
plot(t, v, '.', "MarkerSize", 2);

title("Voltage on Capacitor - Under Damping - RLC Circuit");
annotation("textbox", [.6, .1, .2, .2], "String", "R=11.6\Omega", 'FitBoxToText','on', "FontSize", 19);
xlabel("Time (s)")
ylabel("Voltage (V)");
grid on;
set(gca, "FontSize", 15);

%% Fit to damp sin
path = RESULTS_FOLDER + "low_dampening_11.6Ohm.csv";
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

start_t = 5.6e-06;
start_index = find(abs(t-start_t) <= (100000*eps), 1);
t = t(start_index:end);
v = v(start_index:end);

% Fit to damped sin:
[xData, yData] = prepareCurveData( t, v );

% Set up fittype and options.
ft = fittype( 'a*(exp(-b*t))*(cos(w*t+p))+ e', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [6 5000 0 0.4 2e5];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot
figure;
plot(t, v, '.', "MarkerSize", 4);
hold on;
plot(fitresult);

title("Voltage on Capacitor - Under Damping - RLC Circuit");
annotation("textbox", [.6, .1, .2, .2], "String", "R=11.6\Omega", 'FitBoxToText','on', "FontSize", 19);
xlabel("Time (s)")
ylabel("Voltage (V)");
legend("V", "Fit to damped sin");
grid on;
set(gca, "FontSize", 15);

get_params_and_errors(fitresult);
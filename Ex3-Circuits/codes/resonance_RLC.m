%% Get data
% Chanel1 - Signal Generator V
% Chanel2 - Capacitor V

names = ["freq_1.08M.csv";
 "freq_1.38K.csv";
 "freq_144.5K.csv";
 "freq_17.04K.csv";
 "freq_2.4K.csv";
 "freq_23.78K.csv";
 "freq_3.1M.csv";
 "freq_31.00K.csv";
 "freq_33.25K.csv";
 "freq_33.89K.csv";
 "freq_334.csv";
 % "freq_356.3K.csv";  % This is a bad measurement! 
 "freq_36.92K.csv";
 "freq_43.05K.csv";
 "freq_61.63K.csv";
 "freq_735.csv";
 "freq_8.10K.csv"];
paths = "../results/RLC/Resonance/" + names;

freqs = [1.08e6; 
    1.38e3; 
    144.5e3; 
    17.04e3; 
    2.4e3; 
    23.78e3; 
    3.1e6; 
    31e3; 
    33.25e3; 
    33.89e3; 
    334; 
    % 356.3e3;  % This is a bad measurement! 
    36.92e3; 
    43.05e3; 
    61.63e3; 
    735; 
    8.1e3];

omegas = 2*pi*freqs;


%% Analyze amplitude
[amplitudes, amplitude_errors] = arrayfun(@get_amplitude, paths, omegas);

%% Plot amplitudes
% Fit
[xData, yData] = prepareCurveData( omegas, amplitudes );
ft = fittype( 'V/sqrt((w_e^2-w_0^2)^2 + S*w_e^2)', 'independent', 'w_e', 'dependent', 'a' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [3e8, 2e11, 2e5];
opts.Lower = [0, 0, 0];

[fitresult, gof] = fit( xData, yData, ft, opts );


figure;

errorbar(omegas, amplitudes, amplitude_errors, ".");
hold on;
% plot(fitresult);  % This has bad resolution
fplot(@(a) feval(fitresult, a), xlim);

title("Amplitude vs Excitatory Omega - RLC Circuit");
xlabel("Omega (rad/s)")
ylabel("Amplitude (Voltage)");
grid on;
set(gca, "FontSize", 15);
set(gca, "xScale", "log");


% Find width
[names, vals errs] = get_params_and_errors(fitresult);
F = vals(2); 
s = vals(1);
w0 = vals(3);
s_err = errs(1);
w0_err = errs(3);

x = linspace(1e3, 1e8, 10000);
y = F./sqrt((x.^2-w0.^2).^2 + s*x.^2);
half_max = max(y) / 2;
goods = x(y>half_max); 
width = goods(end) - goods(1);
disp("width:" + width);
hold on;
plot(xlim, [half_max, half_max], '--');% .* ones(1, length(goods)))

[max_y, max_index] = max(y);
disp("Resonance at omega= " + x(max_index));

%% Plot amplitudes - V_db
% Fit
V_in = 3.404;
V_out = amplitudes;
V_out_err = amplitude_errors;
V_db_err = (20./V_out) .* V_out_err;

[xData, yData] = prepareCurveData( omegas, amplitudes );
ft = fittype( 'V/sqrt((w_e^2-w_0^2)^2 + S*w_e^2)', 'independent', 'w_e', 'dependent', 'a' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [3e8, 2e11, 2e5];
opts.Lower = [0, 0, 0];

[fitresult, gof] = fit( xData, yData, ft, opts );

figure;

errorbar(omegas, 20*log(V_out./V_in), V_db_err, ".");
hold on;
% plot(fitresult);  % This has bad resolution

fplot(@(a) 20*log(feval(fitresult, a)/V_in), xlim);

title("Amplitude vs Excitatory Omega - RLC Circuit - 20V\_db");
xlabel("Omega (rad/s)")
ylabel("System's Responsivness");
grid on;
set(gca, "FontSize", 15);
set(gca, "xScale", "log");


%% Analyze phase shift
[phase_shifts, errors] = arrayfun(@get_phase_shift, paths, omegas);

phase_shifts = mod(-phase_shifts+0.2, 2*pi)-0.2;

%{
[omegas, indexes] = sort(omegas);
phase_shifts = phase_shifts(indexes);
omegas = omegas(3:end-2);
phase_shifts = phase_shifts(3:end-2);
errors = errors(3:end-2);
%}

%% Plot phase shift
figure;

errorbar(omegas, phase_shifts, errors, ".");

title("Phase Shift - RLC Circuit");
xlabel("Omega (rad/s)")
ylabel("Phase Shift (radians)");
grid on;
set(gca, "FontSize", 15);
set(gca, "xScale", "log");

%% Plot stable state example:
path = "../results/RLC/Resonance/" + "freq_31.00K.csv";
start_w = 31e3*2*pi;
a = readtable(path);
t = a.Var4;
gen_v = a.Var5;
cap_v = a.Var11;

gen_fit = fit_to_sin(t, gen_v, start_w);
cap_fit = fit_to_sin(t, cap_v, start_w);

figure;
plot(gen_fit, t, gen_v, 'b.');
hold on;
plot(cap_fit, t, cap_v, 'g.');
title("Generated signal and Measured voltage");
legend("Generated V", "Generated V - Fit to sin", "Measured V", "Measured V - Fit to sin"); 
xlabel("Time (s)");
ylabel("Voltage (V)");
set(gca, "FontSize", 15);

function fitresult = fit_to_sin(x, y, start_w)
[xData, yData] = prepareCurveData( x, y );

% Set up fittype and options.
ft = fittype( 'a*sin(w*t+p) + c', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [0 0 -pi start_w/10];
opts.StartPoint = [3 4 0 start_w];
opts.Upper = [100 10 pi start_w*10];


% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

if gof.rsquare >= 0.5
    return;
end

warning("got r-square of " + gof.rsquare + " trying other starting point for fitting");

opts.Lower = [0 -20 -pi start_w/10];
opts.StartPoint = [50 0 0.2 start_w];
opts.Upper = [100 20 pi start_w*10];
[fitresult, gof] = fit( xData, yData, ft, opts );

if gof.rsquare < 0.9
    figure;
    plot(fitresult, x, y);
    title("Expected omega " + start_w);
    error("Still got r-square of " + gof.rsquare + " after using other starting point!");
end

end

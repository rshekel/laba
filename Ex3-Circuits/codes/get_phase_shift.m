function [phase_shift, err] = get_phase_shift(path, start_w)
%% Load data
a = readtable(path);
t = a.Var4;
gen_v = a.Var5;
cap_v = a.Var11;

gen_fit = fit_to_sin(t, gen_v, start_w);
cap_fit = fit_to_sin(t, cap_v, start_w);

phase_shift = cap_fit.p - gen_fit.p;
[~, ~, gen_errs] = get_params_and_errors(gen_fit);
[~, ~, cap_errs] = get_params_and_errors(cap_fit);
gen_p_err = gen_errs(3);
cap_p_err = cap_errs(3);
err = sqrt(gen_p_err^2 + cap_p_err^2);

figure("Name", "KHz: " + start_w /(2*pi) / 1000);
plot(gen_fit, t, gen_v);
hold on;
plot(cap_fit, t, cap_v);
%title("Path: " + path + "; start omega" + start_w);
title("start freq, KHz, " + start_w /(2*pi) / 1000);

end

function [nsigma_1, nsigma_2, nsigma_3, nsigma_tot] = calc_error_for_guess(L_guess,C_guess)
n_sigma = @(a1_1, a1_2, a2_1, a2_2) (abs(a1_1 - a2_1) ./ sqrt(a1_2.^2 + a2_2.^2));
error_prop_sumsub = @(a_err, b_err) sqrt(a_err.^2 + b_err.^2);
error_prop_muldiv = @(z, a1_1, a1_2, a2_1, a2_2) z.*sqrt((a1_2./a1_1).^2 + (a2_2./a2_1).^2);

%% Calc error for RLC underdamped circuit

R_outer = 11.6;
R_outer_err = 0.4;
R_inner = 64.16;
R_inner_err = 0.03;
R = R_inner+R_outer;
R_err = sqrt(R_inner_err^2 + R_outer_err^2);

L = 20.85e-3;
L_err = 0.03e-3;
C = 1e-9;
C_err = 0.03e-9; 

% search for inner unknown L, C
L = L + L_guess; % -14.2e-3; 
C = C + C_guess; % 2.2484e-9; 
sz = size(L_guess);
L_err = ones(size(L_guess)) .* L_err; 
C_err = ones(size(C_guess)) .* C_err; 

b = R./(2.*L); 
b_err = error_prop_muldiv(b, R, R_err, 2.*L, 2.*L_err);

measured_b = 5692;
measured_b_err = 9;
measured_b = measured_b.*ones(sz);
measured_b_err = measured_b_err.*ones(sz);

nsigma_1 = n_sigma(b, b_err, measured_b, measured_b_err);

w = sqrt(1./(C.*L) - R.^2./(4.*L.^2));

w_to_C = 1./(2.*w).*1./L;
w_to_L = 1./(2.*w).*(-C./(L.^2));
w_to_b = 1./(2.*w).*(-2.*b);

w_err = sqrt((w_to_C.^2).*(C_err.^2)+(w_to_L.^2).*(L_err.^2)+(w_to_b.^2).*(b_err.^2));

measured_omega = 215080;
measured_omega_err = 9;

nsigma_2 = n_sigma(w, w_err, measured_omega, measured_omega_err);


%% Calc error for RLC overdamped circuit
l1 = 250000;
l1_err = 7000;
l2 = 194000;
l2_err = 4300;

l1_l2 = l1+l2;
l1_l2_err = error_prop_sumsub(l1_err, l2_err);

R_outer = 8.43e3;
R_outer_err = 0.008.*R_outer + 0.01e3;  % 0.8%+1 digit
R_inner = 64.16;
R_inner_err = 0.03;
R = R_inner+R_outer;
R_err = error_prop_sumsub(R_inner_err, R_outer_err);

expected_R_L = R./L;
expected_R_L_err = error_prop_muldiv(expected_R_L, R, R_err, L, L_err);

nsigma_3 = n_sigma(expected_R_L, expected_R_L_err, l1_l2, l1_l2_err);
nsigma_tot = nsigma_1 + nsigma_2 + nsigma_3;
end

path = "../results/RL/" + "R5.99K_slope.csv";
%% Import data
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);

%{
%% Cut unimportant first part
max_v = max(v);
start_index = find(v < 0.7*max_v);
start_index = start_index(1);
t = t(start_index:end); 
v = v(start_index:end);
%}

%% Fit to exp
fitresult = fit_to_exp(path, 0.7, 0);
get_params_and_errors(fitresult);

%% Plot
% Plot fit with data.
figure;


h = plot( fitresult );
h.LineWidth = 1.5;

hold on;
plot(t, v, '.', "MarkerSize", 2);

title("Voltage on Resistor while discharing - RL Circuit");
legend( 'fit to Ae^{-Bt}', 'Measured Voltage', 'Location', 'NorthEast' );
% Label axes
xlabel("Time (s)")
ylabel("Voltage (V)");
grid on

set(gca, "FontSize", 15);

%{
%% log scale:
set(gca, "YScale", "log");
title("Voltage on Resistor while discharing - RL Circuit - Y log-scale");
%}

%% Residuals 
max_v = max(v);
start_index = find(v < 0.8*max_v);
start_index = start_index(1);
t = t(start_index:end); 
v = v(start_index:end);

figure;
residuals = (v - feval(fitresult, t));
plot(t, residuals, '.');

ylabel("Residuals");
xlabel("Time (sec)");
grid on;
set(gca, "FontSize", 15);
title("Residuals for v - RL circuit");

%% Load data
names = ["R1.13k_slope.csv";
 "R2.00K_slope.csv";
 "R2.99K_slope.csv";
 "R3.98K_slope.csv";
 "R5.03K_slope.csv";
 "R6.00K_slope.csv";
 "R7.03K_slope.csv";
 "R8.03K_slope.csv";
 "R9.01K_slope.csv";
 "R10.06K_slope.csv";];
paths = "../results/RC/" + names;

Rs = [1.13; 2; 2.99; 3.98; 5.03; 6.00; 7.03; 8.03; 9.01; 10.06];
Rs = Rs .* 1000;  % Numbers are in K Ohms

firs = arrayfun(@fit_to_exp, paths, "UniformOutput", false);

[names, values, errors] = cellfun(@get_params_and_errors, firs, "UniformOutput", false);

damping_factors = cellfun(@(t) t(2), values);
damping_factor_errors = cellfun(@(t) t(2), errors);

taus = 1./damping_factors;
tau_errs = damping_factor_errors ./ (damping_factors.^2);

%% Linear fit
[xData, yData] = prepareCurveData( Rs,  taus);

% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );

%% Plot
figure("Name", "Tau vs. R - RC Circuit");

y_err = tau_errs;
x_err = 0.8*0.01*Rs;
errorbar(Rs, taus, y_err, y_err, x_err, x_err, ".");

hold on;
plot(fitresult);

legend("off");
title("Tau vs. Resistance - RC Circuit");
ylabel("\tau (1/s)");
xlabel("Resistance (\Omega)");

get_params_and_errors(fitresult);
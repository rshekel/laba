%% general functions 
n_sigma = @(a1, a2) disp("n sigma: " + (abs(a1(1) - a2(1)) / sqrt(a1(2)^2 + a2(2)^2)));

% error prop for z=x+y; z=x-y.
error_prop_sumsub = @(a_err, b_err) sqrt(a_err^2 + b_err^2);

% error prop for z=x*y; z=x/y. a1=[x, x_err], a2=[y, y_err]
error_prop_muldiv = @(z, a1, a2) z*sqrt((a1(2)./a1(1)).^2 + (a2(2)./a2(1)).^2);

n_sigma([22.76, 0.03], [23.0, 0.3]);

%% calc error for parallel resistance
R1 = 23.0;
R1_err = 0.1;
R2 = 22;
R2_err = 0.3; 

Rtot = 1/((1/R1) + (1/R2));
tot_to_1 = R2^2/(R1+R2)^2; 
tot_to_2 = R1^2/(R1+R2)^2; 

Rtot_err = sqrt((tot_to_1^2 * R1_err^2) + (tot_to_2^2 * R2_err^2)); 
disp("R_tot: " + Rtot + "+-" + Rtot_err);

%% Resistance in line (serial)
R1 = 0.0133;
R1_err = 0.0008; 

Rtot = R1 + R1;
Rtot_err = error_prop_sumsub(R1_err, R1_err);
disp("R_tot: " + Rtot + "+-" + Rtot_err);

%% Calc error for V/I - unused 
I = 92 / 1000;
x_err = 0.5/100 * x;
y = 0.87;
y_err = 0.05;
z = x/y;
z_err = error_prop_muldiv(z, [x, x_err], [y, y_err]);
disp("z: " + z + " +- " + z_err);

%% Calc error for V/I-2
x = 11.6 / 1000;
x_err = 0.06/1000;
y = 0.87;
y_err = 0.05;
z = x/y;
z_err = error_prop_muldiv(z, [x, x_err], [y, y_err]);
disp("z: " + z + " +- " + z_err);

%% Calc error for V/I-3
x = 24.5 / 1000;
x_err = 0.12/1000;
y = 0.85;
y_err = 0.05;
z = x/y;
z_err = error_prop_muldiv(z, [x, x_err], [y, y_err]);
disp("z: " + z + " +- " + z_err);

%% Calc error for RC circuit
x = 2.99e3;
x_err = 0.03 * 10^3;
y = 0.1e-6;
y_err = 0.2e-8;
z = x*y;
z_err = error_prop_muldiv(z, [x, x_err], [y, y_err]);

disp("RC: " + z + " +- " + z_err);

tau = 1/z;
tau_err = error_prop_muldiv(tau, [1, 0], [z, z_err]);
disp("tau: " + tau + " +- " + tau_err);

b = 3222;
b_err = 5;
n_sigma([b, b_err], [tau, tau_err]);

%% Calc error for RL circuit with R~6KOhm:
R = 5.99 * 1000 + 64;
R_err = 0.06 * 1000;
L = 100 * (1/1000);
L_err = 0.05*L;

t = R/L;
t_err = error_prop_muldiv(t, [R, R_err], [L, L_err]);
disp("t: " + t + " +- " + t_err);

b = 68038;
b_err = 166;
n_sigma([t, t_err], [b, b_err]);

%% Calc error for L in RL circuit:
L_measured = 0.0889;
L_measured_err = 0.0007;
L_given = 0.1;
L_given_err = 0.05*L_given;
n_sigma([L_measured, L_measured_err], [L_given, L_given_err]);

%% Calc error for RLC underdamped circuit
disp("----------");
R_outer = 11.6;
R_outer_err = 0.4;
R_inner = 64.16;
R_inner_err = 0.03;
R = R_inner+R_outer;
R_err = sqrt(R_inner_err^2 + R_outer_err^2);

L = 20.85e-3;
L_err = 0.03e-3;
C = 1e-9;
C_err = 0.03e-9; 

% search for inner unknown L, C
% L = L - 14.2e-3; 
% C = C + 2.2484e-9; 

b = R/(2*L);
b_err = error_prop_muldiv(b, [R, R_err], [2*L, 2*L_err]);
disp("Expedted b: " + b + " +- " + b_err);

measured_b = 5692;
measured_b_err = 9;
disp("measured b: " + measured_b + " +- " + measured_b_err); 

n_sigma([b, b_err], [measured_b, measured_b_err])


w = sqrt(1/(C*L) - R^2/(4*L^2));

w_to_C = 1/(2*w*L)*(-1/C^2);
w_to_L = 1/(2*w)*(-C/(L^2) + (R^2)/(2*L^3));
w_to_R = 1/(2*w)*(-R/(2*L^2));

w_err = sqrt((w_to_C^2)*(C_err^2)+(w_to_L^2)*(L_err^2)+(w_to_R^2)*(R_err^2));
disp("Expedted omega: " + w + " +- " + w_err);

measured_omega = 215080;
measured_omega_err = 9;

disp("measured omega: " + measured_omega + " +- " + measured_omega_err); 
n_sigma([w, w_err], [measured_omega, measured_omega_err])


%% Calc error for RLC overdamped circuit
disp("--------");
l1 = 250000;
l1_err = 7000;
l2 = 194000;
l2_err = 4300;

l1_l2 = l1+l2;
l1_l2_err = error_prop_sumsub(l1_err, l2_err);
disp("l1+l2 = " + l1_l2 + " +- " + l1_l2_err);

R_outer = 8.43e3;
R_outer_err = 0.008*R_outer + 0.01e3;  % 0.8%+1 digit
R_inner = 64.16;
R_inner_err = 0.03;
R = R_inner+R_outer;
R_err = error_prop_sumsub(R_inner_err, R_outer_err);
disp("R = " + R + " +- " + R_err);

L = 20.85e-3;
L_err = 0.03e-3;

expected_R_L = R/L;
expected_R_L_err = error_prop_muldiv(expected_R_L, [R, R_err], [L, L_err]);
disp("Expected R/L = " + expected_R_L + " +- " + expected_R_L_err);

disp("N-sigma for R/L")
n_sigma([expected_R_L, expected_R_L_err], [l1_l2, l1_l2_err]);

%% Calcs for HWFM
L = 20.85e-3;
L_err = 0.03e-3;

%L = L/5;

R_outer = 11.6;
R_outer_err = 0.4;
R_inner = 64.16;
R_inner_err = 0.03;
R = R_inner+R_outer;
R_err = sqrt(R_inner_err^2 + R_outer_err^2);

W = R / L;
W_err = error_prop_muldiv(W, [R, R_err], [L, L_err]);
disp("Expected W = " + W + " +- " + W_err);

%% Error for resonance 
R_outer = 11;
R_outer_err = 0.4;
R_inner = 64.16;
R_inner_err = 0.03;
R = R_inner+R_outer;
R_err = sqrt(R_inner_err^2 + R_outer_err^2);

L = 20.85e-3;
L_err = 0.03e-3;
C = 1e-9;
C_err = 0.03e-9; 

w = sqrt(1/(C*L) - R^2/(2*L^2));

w_to_C = 1/(2*w*L)*(-1/C^2);
w_to_L = 1/(2*w)*(-C/(L^2) + (R^2)/(L^3));
w_to_R = 1/(2*w)*(-R/(L^2));

w_err = sqrt((w_to_C^2)*(C_err^2)+(w_to_L^2)*(L_err^2)+(w_to_R^2)*(R_err^2));

f = w / (2*pi); 
f_err = w_err / (2*pi);

disp("Expedted w: " + w + " +- " + w_err);
disp("Expedted frequency: " + f + " +- " + f_err);

measured_frequency = 33.5e3;
measured_frequency_err = 0;

disp("measured frequency: " + measured_frequency + " +- " + measured_frequency_err); 
n_sigma([f, f_err], [measured_frequency, measured_frequency_err]);

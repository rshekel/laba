%% Load data
names = ["R1.03K_slope.csv"
 "R2.05K_slope.csv"
 "R3.01K_slope.csv"
 "R4.02K_slope.csv"
 "R5.03K_slope.csv"
 "R5.99K_slope.csv"
 "R7.01K_slope.csv"
 "R8.03K_slope.csv"
 "R9.00K_slope.csv"
 "R10.00K_slope.csv"];
paths = "../results/RL/" + names;

Rs = [1.03; 2.05; 3.01; 4.02; 5.03; 5.99; 7.01; 8.03; 9.00; 10.00];
Rs = Rs .* 1000;  % Numbers are in K Ohms

IGNORE_V_ABOVE = 0.7;
IGNORE_V_BELOW = 0.1;

firs = arrayfun(@(f) fit_to_exp(f, IGNORE_V_ABOVE, IGNORE_V_BELOW), paths, "UniformOutput", false);

[names, values, errors] = cellfun(@get_params_and_errors, firs, "UniformOutput", false);

damping_factors = cellfun(@(t) t(2), values);
damping_factor_errors = cellfun(@(t) t(2), errors);

taus = 1./damping_factors;
tau_errs = damping_factor_errors ./ (damping_factors.^2);

%% Fit to tau=L/R
[xData, yData] = prepareCurveData( Rs,  taus);

% Set up fittype and options.
ft = fittype( 'L/R', 'independent', 'R', 'dependent', 'Tau' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = 1;
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

%% Plot
figure("Name", "Tau vs. R - RL Circuit");

plot(fitresult);
hold on;

y_err = tau_errs;
% TODO: What is x_err? (should be error in measured R)
x_err = zeros(1, length(taus));
errorbar(Rs, taus, y_err, ".");

title("Tau vs. Resistance - RL Circuit");
legend("Fit to \tau=L/R");
ylabel("\tau (1/s)");
xlabel("Resistance (\Omega)");
grid on;
set(gca, "FontSize", 15);

get_params_and_errors(fitresult);

%{
%% loglog scale
title("Tau vs. Resistance - RL Circuit - loglog scale");
set(gca, "xScale", "log");
set(gca, "YScale", "log");
%}

%% Plot residuals:
figure;
residuals = (taus - feval(fitresult, Rs))./tau_errs;
bar(Rs, residuals);

ylabel("Normalized Residual");
xlabel("Resistance (\Omega)");
grid on;
set(gca, "FontSize", 15);
title("Normalized Residuals for \tau=L/R");

%{
%% Linear fit to damping_factor(1/tau) vs R

[xData, yData] = prepareCurveData( Rs,  damping_factors);

% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );

figure;
plot(fitresult);
hold on;
errorbar(Rs, damping_factors, damping_factor_errors, '.');
%}
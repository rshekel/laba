%% Load data
V_over_I = load("..\results\R\signal_gen_inner_resistance.txt");

Vs = V_over_I(:,1);
Is = V_over_I(:,2);
Is = Is ./ 1000;
Rs = Vs ./ Is;

%% Plot
figure;

% Using 0.5% error
%x_err = 0.005.*Is;
%y_err = 0.005.*Vs;
% TODO: Calc error!
%errorbar(Rs, Vs, y_err, y_err, x_err, x_err, '.');
plot(Rs, Vs, ".");
hold on;

title("Voltage (V) vs. Resistance (\Omega)");
xlabel("Resistance (\Omega)");
ylabel("Voltage (V)");
grid on

V_gen = 1.184;
plot(xlim, [V_gen, V_gen], '--r');
legend("measured Voltage", "Sig. Generator Voltage", "Location", "SouthEast");

%% Linear fit of (voltage diff between R and the signal generator) vs I current
v_gen = 1.184;
v_diffs = v_gen - Vs;

[xData, yData] = prepareCurveData( Is,  v_diffs);

% Set up fittype and options.
ft = fittype( 'poly1' );
% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );

get_params_and_errors(fitresult);

%% Plot voltage diff between R and the signal generator

figure;
plot(fitresult);
hold on;
plot(Is, v_diffs, '.');
title("Current on outer Resistor vs. Voltage difference");
xlabel("Current (A)");
ylabel("Voltage diff (V)");
grid on;
legend("off");

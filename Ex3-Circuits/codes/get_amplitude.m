function [amplitude, err] = get_amplitude(path, start_w)
%% Load data
a = readtable(path);
t = a.Var10;
v = a.Var11;

v_fit = fit_to_sin(t, v, start_w);
amplitude = v_fit.a;
[~, ~, v_errs] = get_params_and_errors(v_fit);
err = v_errs(1);

figure;
plot(v_fit, t, v);
title("Path: " + path + "; start \omega=" + start_w);
end

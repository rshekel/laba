close all;

%% Plot for phi<<pi/2:
path = "../results/RLC/Resonance/" + "freq_23.78K.csv";
start_w = 23.78e3*2*pi;
a = readtable(path);
t = a.Var4;
gen_v = a.Var5;
cap_v = a.Var11;

gen_fit = fit_to_sin(t, gen_v, start_w);
cap_fit = fit_to_sin(t, cap_v, start_w);

figure;
plot(gen_fit, t, gen_v, 'b.');
hold on;
plot(cap_fit, t, cap_v, 'g.');
title("Generated signal and Measured voltage f=23.78KHz");
legend("Generated V", "Generated V - Fit to sin", "Measured V", "Measured V - Fit to sin"); 
xlabel("Time (s)");
ylabel("Voltage (V)");
set(gca, "FontSize", 15);

%% Plot for phi~pi/2:
path = "../results/RLC/Resonance/" + "freq_33.25K.csv";
start_w = 33.25e3*2*pi;
a = readtable(path);
t = a.Var4;
gen_v = a.Var5;
cap_v = a.Var11;

gen_fit = fit_to_sin(t, gen_v, start_w);
cap_fit = fit_to_sin(t, cap_v, start_w);

figure;
plot(gen_fit, t, gen_v, 'b.');
hold on;
plot(cap_fit, t, cap_v, 'g.');
title("Generated signal and Measured voltage f=33.25KHz");
legend("Generated V", "Generated V - Fit to sin", "Measured V", "Measured V - Fit to sin"); 
xlabel("Time (s)");
ylabel("Voltage (V)");
set(gca, "FontSize", 15);

%% Plot for phi>>pi/2:
path = "../results/RLC/Resonance/" + "freq_36.92K.csv";
start_w = 36.92e3*2*pi;
a = readtable(path);
t = a.Var4;
gen_v = a.Var5;
cap_v = a.Var11;

gen_fit = fit_to_sin(t, gen_v, start_w);
cap_fit = fit_to_sin(t, cap_v, start_w);

figure;
plot(gen_fit, t, gen_v, 'b.');
hold on;
plot(cap_fit, t, cap_v, 'g.');
title("Generated signal and Measured voltage f=36.92KKHz");
legend("Generated V", "Generated V - Fit to sin", "Measured V", "Measured V - Fit to sin"); 
xlabel("Time (s)");
ylabel("Voltage (V)");
set(gca, "FontSize", 15);


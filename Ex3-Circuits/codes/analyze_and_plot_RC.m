path = "../results/RC/" + "R2.99K_slope.csv";
%% Import data
a = importdata(path);
t = a.data(:, 1);
v = a.data(:, 2);


%% Cut unimportant first part
max_v = max(v);
start_index = find(v < 0.9*max_v);
start_index = start_index(1);
t = t(start_index:end); 
v = v(start_index:end);


%% Fit to exp
fitresult = fit_to_exp(path);

%% Plot
% Plot fit with data.
figure;

plot( fitresult );
hold on;
plot(t, v, '.', "MarkerSize", 2);

title("Voltage on Capacitor while discharing");
legend("off");
%legend( h, 'v vs. t', 'untitled fit 1', 'Location', 'NorthEast' );
% Label axes
xlabel("Time (s)")
ylabel("Voltage (V), log");
grid on

set(gca, "YScale", "log");
title("Voltage on Capacitor while discharing - Y log scale");

%set(gca, "XScale", "log");

function finish_index = find_finish_index(path)

data = ImportCapstoneFile(path);

times = data.Times; 
positions = data.Positionm; 

time_interval = times(2) - times(1); % 0.025 


maxes = islocalmax(positions);
max_indexes = find(maxes);
differences = diff(max_indexes);
omegas = (2*pi) ./ (differences * time_interval);

% We find the finish time as a big change in omega
omegas_diff = diff(omegas);
OMEGA_DIFF_THRESHOLD = 7;
bad_omegas_indexes = find(omegas_diff > OMEGA_DIFF_THRESHOLD);
% We want to include ALL omegas in the first third, no matter what
% (In 0.085-1 there is a deviation after only 5 oscillations, that we want to ignore).
bad_omegas_indexes = bad_omegas_indexes(bad_omegas_indexes > length(omegas)/3);

if length(bad_omegas_indexes) >= 1
    last_max_index = max_indexes(bad_omegas_indexes(1));
else
    last_max_index = max_indexes(end);
end

finish_index = last_max_index;
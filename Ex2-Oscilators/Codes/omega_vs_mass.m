paths = [
"..\Results\Dampening\0.00kg\1\0.00kg_dampening1.csv";
"..\Results\Dampening\0.00kg\2\0.00kg_dampening2.csv";

"..\Results\Dampening\0.01kg\1\0.01kg_dampening1.csv";
"..\Results\Dampening\0.01kg\2\0.01kg_dampening2.csv";

"..\Results\Dampening\0.03kg\1\0.03kg_dampening1.csv";
"..\Results\Dampening\0.03kg\2\0.03kg_dampening2.csv";

"..\Results\Dampening\0.05kg\1\0.05kg_dampening.csv";
"..\Results\Dampening\0.05kg\2\0.05kg_dampening2.csv";
"..\Results\Dampening\0.05kg\3\0.05kg_dampening3.csv";

"..\Results\Dampening\0.07kg\1\0.07kg_dampening.csv";
"..\Results\Dampening\0.07kg\2\0.07kg_dampening2.csv";

"..\Results\Dampening\0.085kg\1\0.085kg_dampening1.csv";
"..\Results\Dampening\0.085kg\2\0.085kg_dampening2.csv";
];
paths = paths';

% base_mass = 0.067;
% base_mass_err = 0.0005;
base_mass = 0.067 + 0.0063;
base_mass_err = 0.001;

% For plotting all results
masses = [0, 0, 0.01, 0.01, 0.03, 0.03, 0.05, 0.05, 0.05, 0.07, 0.07, 0.085, 0.085];
masses = masses + base_mass;

f = @(t) get_omega(t);  % I have no idea why it's necessary
[omegas, omega_errs] = arrayfun(f, paths);

omegas_matrix = [omegas(1), omegas(2), 0;
    omegas(3), omegas(4), 0;
    omegas(5), omegas(6), 0;
    omegas(7), omegas(8), omegas(9);
    omegas(10), omegas(11), 0;
    omegas(12), omegas(13), 0];
x_err = base_mass_err .* ones(length(masses), 1);
omega_errs_matrix = [omega_errs(1), omega_errs(2), 0;
    omega_errs(3), omega_errs(4), 0;
    omega_errs(5), omega_errs(6), 0;
    omega_errs(7), omega_errs(8), omega_errs(9);
    omega_errs(10), omega_errs(11), 0;
    omega_errs(12), omega_errs(13), 0];


% Plotting all omegas, with repetition of the same mass
figure( 'Name', 'Omega vs Mass (with several runs)' );
unique_masses = unique(masses);
bar(unique_masses, omegas_matrix);
title( 'Omega vs Mass, for several runs' );
xlabel('mass (kg)');
ylabel('\omega (1/s)') ;
set(gca, 'fontsize', 18);


% Analyzing for selected runs
selected_indexes = [1, 4, 5, 7, 10, 13];
masses = masses(selected_indexes);
omegas = omegas(selected_indexes);
omega_errs = omega_errs(selected_indexes);
x_err = x_err(selected_indexes);

% Fit: 'omega vs mass'.
[xData, yData] = prepareCurveData( masses, omegas );

% Fit to sqrt
ft = fittype( 'sqrt(k/m)', 'independent', 'm', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [0];
opts.StartPoint = [50];
opts.Upper = [200];

[sqrt_fit, gof] = fit( xData, yData, ft, opts );

figure( 'Name', 'Fit Omega vs Mass' );
errorbar(masses, omegas, omega_errs, omega_errs, x_err, x_err, '.');
hold on;
axis([0, 0.2, 0, 60]);
plot(sqrt_fit);
title( 'Omega vs Mass' );
legend( 'Omega with error', 'Fit to $$ \sqrt{\frac{k}{m}} $$', 'Interpreter', 'latex');
xlabel('mass (kg)');
ylabel('\omega (1/s)') ;
set(gca, 'fontsize', 18);


% Set up fittype and options.
ft = fittype( 'sqrt(k/m-(a^2)/(4*m^2))', 'independent', 'm', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [1 20]; % Forcing alpha to be at least 1, to see its behavior
opts.StartPoint = [1 50];
opts.Upper = [Inf 100];


% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

[params_name, values, errors] = get_params_and_errors(fitresult);
a = values(1);
a_err = errors(1);
k = values(2);
k_err = errors(2);

figure( 'Name', 'Fit Omega vs Mass' );
errorbar(masses, omegas, omega_errs, omega_errs, x_err, x_err, '.');
hold on;
axis([0, 0.2, 0, 60]);
plot(fitresult);
title( 'Omega vs Mass' );
legend( 'Omega with error', 'Fit to $$ \sqrt{\frac{k}{m}-\frac{\alpha^2}{4m^2}} $$', 'Interpreter', 'latex');
xlabel('mass (kg)');
ylabel('\omega (1/s)') ;
set(gca, 'fontsize', 18);

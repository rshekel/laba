close all;

[w0, w0_err, s, s_err] = amplitude_vs_excitatory_freq('..\results\DrivenResonance\spring1_m0', "Mass=73.3 (gram), Spring no.1");
[w0, w0_err, s, s_err] = amplitude_vs_excitatory_freq('..\results\DrivenResonance\spring1_m0.05kg', "Mass=123.3 (gram), Spring no.1");

[w0, w0_err, s, s_err] = amplitude_vs_excitatory_freq('..\results\DrivenResonance\spring2_m0', "Mass=73.3 (gram), Spring no.2");
% The peak in this graph is too high, so:
axis([2.5, 14, -0.005, 0.04]);

m2 = 67 + 20 + 4 + 1.83;
[w0, w0_err, s, s_err] = amplitude_vs_excitatory_freq('..\results\DrivenResonance\spring3_m0.02kg', "Mass=92.8 (gram), Spring no.2");
[w0, w0_err, s, s_err] = amplitude_vs_excitatory_freq('..\results\DrivenResonance\spring3_m0.07kg', "Mass=142.8 (gram), Spring no.2");

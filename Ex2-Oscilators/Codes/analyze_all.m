[fr001, gof001]=analyze_dampening("..\Results\Dampening\0.00kg\1\0.00kg_dampening1.csv");
[fr002, gof002]=analyze_dampening("..\Results\Dampening\0.00kg\2\0.00kg_dampening2.csv");

[fr011, gof011]=analyze_dampening("..\Results\Dampening\0.01kg\1\0.01kg_dampening1.csv");
[fr012, gof012]=analyze_dampening("..\Results\Dampening\0.01kg\2\0.01kg_dampening2.csv");

[fr031, gof031]=analyze_dampening("..\Results\Dampening\0.03kg\1\0.03kg_dampening1.csv");
[fr032, gof032]=analyze_dampening("..\Results\Dampening\0.03kg\2\0.03kg_dampening2.csv");

[fr051, gof051]=analyze_dampening("..\Results\Dampening\0.05kg\1\0.05kg_dampening.csv");
[fr052, gof052]=analyze_dampening("..\Results\Dampening\0.05kg\2\0.05kg_dampening2.csv");
[fr053, gof053]=analyze_dampening("..\Results\Dampening\0.05kg\3\0.05kg_dampening3.csv");

[fr071, gof071]=analyze_dampening("..\Results\Dampening\0.07kg\1\0.07kg_dampening.csv");
[fr072, gof072]=analyze_dampening("..\Results\Dampening\0.07kg\2\0.07kg_dampening2.csv");

[fr0851, gof0851]=analyze_dampening("..\Results\Dampening\0.085kg\1\0.085kg_dampening1.csv");
[fr0852, gof0852]=analyze_dampening("..\Results\Dampening\0.085kg\2\0.085kg_dampening2.csv");

close all;

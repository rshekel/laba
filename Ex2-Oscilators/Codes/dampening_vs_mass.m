paths = [
"..\Results\Dampening\0.00kg\1\0.00kg_dampening1.csv";
"..\Results\Dampening\0.00kg\2\0.00kg_dampening2.csv";

"..\Results\Dampening\0.01kg\1\0.01kg_dampening1.csv";
"..\Results\Dampening\0.01kg\2\0.01kg_dampening2.csv";

"..\Results\Dampening\0.03kg\1\0.03kg_dampening1.csv";
"..\Results\Dampening\0.03kg\2\0.03kg_dampening2.csv";

"..\Results\Dampening\0.05kg\1\0.05kg_dampening.csv";
"..\Results\Dampening\0.05kg\2\0.05kg_dampening2.csv";
"..\Results\Dampening\0.05kg\3\0.05kg_dampening3.csv";

"..\Results\Dampening\0.07kg\1\0.07kg_dampening.csv";
"..\Results\Dampening\0.07kg\2\0.07kg_dampening2.csv";

"..\Results\Dampening\0.085kg\1\0.085kg_dampening1.csv";
"..\Results\Dampening\0.085kg\2\0.085kg_dampening2.csv";
];
paths = paths';

base_mass = 0.067 + 0.0063;
base_mass_err = 0.001;

% For plotting all results
masses = [0, 0, 0.01, 0.01, 0.03, 0.03, 0.05, 0.05, 0.05, 0.07, 0.07, 0.085, 0.085];
masses = masses + base_mass;

f = @(p) get_dampening_factor(p);  % I have no idea why it's necessary
[dampening_factor, dampening_factor_err] = arrayfun(f, paths);

x_err = base_mass_err .* ones(length(masses), 1);

% Plotting all factors, with repetition of the same mass
figure( 'Name', 'Dampening factor (b) vs Mass (with several runs)' );
errorbar(masses, dampening_factor, dampening_factor_err, dampening_factor_err, '.');

title( 'Dampening factor (b) vs Mass, for several runs' );
xlabel('mass (kg)');
ylabel('Dampening factor (b) (1/s)') ;
set(gca, 'fontsize', 18);

alpha = dampening_factor .* (2.*masses);
alpha_err = alpha .* sqrt((dampening_factor_err./dampening_factor).^2 + (base_mass_err./masses).^2);

figure;
errorbar(masses, alpha, alpha_err, alpha_err, '.');

% Plotting all factor, with repetition of the same mass
figure( 'Name', 'alpha viscous factor vs Mass (with several runs)' );
errorbar(masses, alpha, alpha_err, alpha_err, '.');
title( '\alpha viscous factor vs Mass, for several runs' );
xlabel('mass (kg)');
ylabel('\alpha viscous factor (kg/s)') ;
set(gca, 'fontsize', 18);

%{
% Analyzing for selected runs
selected_indexes = [1, 4, 5, 7, 10, 13];
masses = masses(selected_indexes);
alpha = alpha(selected_indexes);
alpha_err = alpha_err(selected_indexes);
%}
figure;
[xData, yData] = prepareCurveData( masses, alpha );
ft = fittype( '0*m + c', 'independent', 'm', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [1];
[fitresult, gof] = fit( xData, yData, ft, opts );
errorbar(masses, alpha, alpha_err, alpha_err, '.');
hold on;
plot(fitresult);
title( '\alpha viscous factor vs Mass' );
xlabel('mass (kg)');
ylabel('\alpha viscous factor (kg/s)') ;
set(gca, 'fontsize', 18);
legend('\alpha with error', 'fit to constant value');

function fit_amplitude_dampening(path, relevant_time) 

path = "..\Results\Dampening\0.05kg\3\0.05kg_dampening3.csv";
relevant_time = 9.5; 

data = ImportCapstoneFile(path);

relevant_indexes = find(data.Times < relevant_time);
times = data.Times(relevant_indexes); 
positions = data.Positionm(relevant_indexes); 
log_positions = log(positions);

maxes = islocalmax(positions); 
max_indexes = find(maxes);

% Fit: 'dampening'.
[xData, yData] = prepareCurveData( times(max_indexes), positions(max_indexes) );

% Set up fittype and options.
ft = fittype( 'a*(exp(-b*t)) + c', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.0965963405306288 0.0904250807567827 0.0344460805029088];

% Fit model to data.
[fr, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'Fit to Damped oscilator equation' );
h = plot( fr, xData, yData );

hold on; 
plot(times, positions, 'b'); 
y_err = 0.001*ones(length(max_indexes),1); 
errorbar(times(max_indexes), positions(max_indexes), y_err, y_err, 'r*');

xlabel("time (s)")
ylabel("positions (m)")
grid on
set(gca, 'fontsize', 18)

legend( h, "positions vs. times", "Fit to a*e^{-bx} +c", 'Location', 'NorthEast' );
title("Fit on peaks to dampening exponent"); 

end 
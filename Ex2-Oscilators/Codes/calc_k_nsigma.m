k1 = 54.1;
k1_err = 0.17;
k2 = 54.36;
k2_err = 0.12;
k_n_sigma = abs(k1 - k2) / sqrt(k1_err^2 + k2_err^2);

w1 = 26.39;
w1_err = 0;
w2 = 26.6;
w2_err = 0.63;
w_n_sigma = abs(w1 - w2) / sqrt(w1_err^2 + w2_err^2);
disp(w_n_sigma); 

a1 = 0.036;
a1_err = 0.002; 
a2 = 0.05;
a2_err = 0.03; 
a_n_sigma = abs(a1 - a2) / sqrt(a1_err^2 + a2_err^2);
disp(a_n_sigma); 

n_sigma = @(a1, a2) disp((abs(a1(1) - a2(1)) / sqrt(a1(2)^2 + a2(2)^2)));
n_sigma([27.06 0.01], [27.23 0.19])

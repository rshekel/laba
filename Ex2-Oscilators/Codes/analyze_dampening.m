function [fitresult, gof] = analyze_dampening(filepath) 
data = ImportCapstoneFile(filepath);

finished_index = find_finish_index(filepath);
times = data.Times(1:finished_index);
positions = data.Positionm(1:finished_index); 
% plot(a.Times, a.Positionm(1:finished), '.');

% Fit: 'dampening'.
[xData, yData] = prepareCurveData( times, positions );

% Set up fittype and options.
ft = fittype( 'a*(exp(-b*t))*(cos(w*t+p))+ e', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.0965963405306288 0.0904250807567827 0.0344460805029088 0.438744359656398 20];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

%{
% Plot fit with data.
figure( 'Name', 'Fit to Damped oscilator equation' );
h = plot( fitresult, xData, yData );
legend( h, 'positions vs. times', 'Fit to dampened oscilator', 'Location', 'NorthEast' );
% Label axes
xlabel("time (s)")
ylabel("positions (m)")
grid on
set(gca, 'fontsize', 18)
%}
end
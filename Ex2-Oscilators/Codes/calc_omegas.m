function omegas = calc_omegas(path, relevant_time) 

data = ImportCapstoneFile(path);

relevant_indexes = find(data.Times < relevant_time);
times = data.Times(relevant_indexes); 
positions = data.Positionm(relevant_indexes); 
time_interval = times(2) - times(1); % 0.025 

maxes = islocalmax(positions); 
max_indexes = find(maxes);
differences = diff(max_indexes);
omegas = (2*pi) ./ (differences * time_interval);

end 
close all;

g = 9.81;

length_over_mass = load("..\results\Spring2KCalc\length_over_mass.txt");
length_error = 0.001;
masses = length_over_mass(:,1);

lengths = length_over_mass(:,2);
rest_length = lengths(1);
lengths = rest_length - lengths;

length_err = ones(length(length_over_mass), 1)*length_error;

% Fit: 'Length over Mass'.
[xData, yData] = prepareCurveData( masses , lengths );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
% k = g*(1/fitresult.p1);

confidence_bounds = confint(fitresult);
fit_errs = (confidence_bounds(2,:) - confidence_bounds(1,:)) / 4;

a = fitresult.p1;
b = fitresult.p2;
l = linspace(-0.005,0.15, 300);

errorbar(masses, lengths, length_err, length_err, '.');
title("Spring Length over Mass");
xlabel('Mass (kg)');
ylabel('Difference from rest length (m)');
grid on;
%axis([-0.005, 0.1, -5e-3, 20e-3]);

hold on;
plot(l, a.*l+b);

% Plotting mass over length - to get k spring factor
figure;

% Fit: 'Mass over Length'.
[xData, yData] = prepareCurveData( lengths , masses );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft );
k = fitresult.p1 * g;

confidence_bounds = confint(fitresult);
fit_errs = (confidence_bounds(2,:) - confidence_bounds(1,:)) / 4;
k_err = fit_errs(1) * g;

a = fitresult.p1;
b = fitresult.p2;
l = linspace(-1e-3,18e-2, 300);

y_err = zeros(length(masses),1);
errorbar(lengths, masses, y_err, y_err, length_err, length_err, '.');
title("Mass over Spring Length");
grid on;
xlabel('Difference from rest length (m)');
ylabel('Mass (kg)');

hold on;
plot(l, a.*l+b);

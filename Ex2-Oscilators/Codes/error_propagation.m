k = 54.1;
b = 0.138;
m = 0.1233;

b_err = 0.024;
k_err = 0.17;
m_err = 0.001;

omega = sqrt(k/m-b^2);
measured_omega = 20.99;
measured_omega_err = 0.0025;

w_to_k = 1/(2*omega)*1/m;
w_to_m = 1/(2*omega)*(-k/(m^2));
w_to_b = 1/(2*omega)*(-2*b);

omega_err = sqrt((w_to_k^2)*(k_err^2)+(w_to_m^2)*(m_err^2)+(w_to_b^2)*(b_err^2));
disp(omega_err);

n_sigma = abs(omega - measured_omega) / sqrt(omega_err^2 + measured_omega_err^2);
disp(n_sigma);

% Error propagation 2
s = 0.0647;
s_err = 0.0108; 
m = 0.1428; 
m_err = 0.001; 

a = sqrt(s) * m;
a_err = sqrt((m/(2*sqrt(s)))^2 * (s_err)^2 + s * (m_err)^2);
disp("a: " + a + "+-" + a_err); 


% Error propagation 3
k = 6.89;
k_err = 0.027;
m = 0.1428;
m_err = 0.001; 

w0 = sqrt(k/m); 
w0_err = (1/(2*m*w0))^2 * (k_err)^2 + (k/(2*m^2*w0))^2 * (m_err)^2; 
w0_err = sqrt(w0_err);
disp("w0: " + w0 + "+-" + w0_err); 

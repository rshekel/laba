function [omega, omega_err] = get_omega(path)
[fr, gof]=analyze_dampening(path);

omega = fr.w;
confidence = confint(fr);
w_confidence = confidence(:, 5);  % 5 is the index of omega
omega_err = (w_confidence(2) - w_confidence(1)) / 2;

end


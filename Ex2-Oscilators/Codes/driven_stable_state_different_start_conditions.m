paths = [
    "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_0_v0_0_1.csv";
    "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_0_v0_0_2.csv";
    
    "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_3_v0_0_1.csv";
    "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_3_v0_0_2.csv";
    
    "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_8_v0_0_1.csv";
    "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_8_v0_0_2.csv";
    
    "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_8_v0_0_2.csv";
    ];

paths = paths'; 

f = @(t) get_omega_and_amplitude(t);  % I have no idea why it's necessary
[omegas, omega_errs, amplitudes, amplitude_errs] = arrayfun(f, paths);

figure;
hold on;
x = 1:length(paths);

b1 = bar(x(1:2), omegas(1:2));
set(b1, 'FaceColor', 'b');
b1 = bar(x(3:4), omegas(3:4));
set(b1, 'FaceColor', 'g');
b1 = bar(x(5:6), omegas(5:6));
set(b1, 'FaceColor', 'm');
b1 = bar([x(7)], [omegas(7)]);
set(b1, 'FaceColor', 'c');

er = errorbar(x,omegas, omega_errs, omega_errs);    
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
title("\omega with different starting conditions"); 
xlabel("Different starting conditions");
ylabel("\omega (1/s)")
set(gca, 'fontsize', 18);
hold off

figure; 
hold on;
x = 1:length(paths);

b1 = bar(x(1:2), amplitudes(1:2));
set(b1, 'FaceColor', 'b');
b1 = bar(x(3:4), amplitudes(3:4));
set(b1, 'FaceColor', 'g');
b1 = bar(x(5:6), amplitudes(5:6));
set(b1, 'FaceColor', 'm');
b1 = bar([x(7)], [amplitudes(7)]);
set(b1, 'FaceColor', 'c');


hold on
er = errorbar(x, amplitudes, amplitude_errs, amplitude_errs);    
er.Color = [0 0 0];                            
er.LineStyle = 'none';  
title("Amplitude with different starting conditions"); 
xlabel("Different starting conditions");
ylabel("Amplitude (cm)")
set(gca, 'fontsize', 18);
% TODO: axes(...) 
hold off

final_omega = sum(omegas./omega_errs.^2) / sum(1./omega_errs.^2);
final_omega_err = 1./sqrt(sum(1./omega_errs.^2)); 
disp(final_omega + "+-" + final_omega_err);
path = "..\Results\Dampening\0.05kg\3\0.05kg_dampening3.csv";
relevant_time = 9.5; 

data = ImportCapstoneFile(path);

relevant_indexes = find(data.Times < relevant_time);
times = data.Times(relevant_indexes); 
positions = data.Positionm(relevant_indexes); 
% Removing equilibrium height, to fit to a*e^(-bt), without c param
positions = positions - 0.2288;
log_positions = log(positions);

maxes = islocalmax(positions); 
max_indexes = find(maxes);
max_times = times(max_indexes);
max_positions = positions(max_indexes);

% Fit: 'dampening'.
[xData, yData] = prepareCurveData( max_times, max_positions );

% Set up fittype and options.
ft = fittype( 'a*(exp(-b*t))+c', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.0965963405306288 0.0904250807567827, 0.3];

% Fit model to data.
[fr, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'Fit to Damped oscilator equation' );
% h = plot( fr, xData, yData );
h = plot( fr, 'b' );

hold on; 
% plot(times, positions, 'b'); 
y_err = 0.001*ones(length(max_indexes),1); 
errorbar(times(max_indexes), positions(max_indexes), y_err, y_err, 'r.');

xlabel("time (s)");
ylabel("positions (m), log scale");
grid on;
set(gca, 'fontsize', 18);
set(gca, 'YScale', 'log');

% legend( h, "positions vs. times", "Fit to a*e^{-bx} +c", 'Location', 'NorthEast' );
legend( "Fit to a*e^{-bx}", "Peaks", 'Location', 'NorthEast' );
title("Fit on peaks to dampening exponent");

figure;
% plot residuals:
expected = feval(fr, max_times);
measured = max_positions;
residuals = measured - expected;
bar(residuals);
title("Residuals for amplitude");
ylabel("residuals (m)");
grid on;
set(gca, 'fontsize', 18);

function [params_name, values, errors] = get_params_and_errors(fitresult)
params_name = coeffnames(fitresult);
values = coeffvalues(fitresult);
confidence_bounds = confint(fitresult);
errors = (confidence_bounds(2,:) - confidence_bounds(1,:)) / 4;
end

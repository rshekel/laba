function [w0, w0_err, s, s_err] = amplitude_vs_excitatory_freq(folder, text)

% folder = '..\results\DrivenResonance\spring3_m0.02kg';
% text = "foo";

measurement_files = dir(folder);
measurement_files = measurement_files(3:end);  % Exclude '.', '..' files

total_files = length(measurement_files) - 1;
freqs = zeros(1, total_files);
paths = cell(1, total_files);
for i=1:length(measurement_files)
    file = measurement_files(i);
    name = file.name;
    if name == "." || name == ".."
        continue;
    elseif name == "omega_no_drive.csv"
        continue;
    end
    % file name for example: 'freq_e_2.5.csv'
    freq_str = regexp(name, 'freq_e_(.*).csv', 'tokens', 'once');
    freq = str2double(freq_str);
    freqs(i) = freq;
    paths{i} = [folder '\' name];
end

[freqs, indexes] = sort(freqs);
paths = paths(indexes);

amps = zeros(1, length(paths));
amp_errs = zeros(1, length(paths));
for i=1:length(paths)
    path = paths{i};
    freq = freqs(i);
    [w w_err a a_err] = get_omega_and_amplitude(path);
    amps(i) = a;
    amp_errs(i) = a_err;
end



omega_e = freqs * 2 * pi;
% Fit
[xData, yData] = prepareCurveData( omega_e, amps );
ft = fittype( 'F/sqrt((w_e^2-w_0^2)^2 + S*w_e^2)', 'independent', 'w_e', 'dependent', 'a' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.5, 0.5, 20];
opts.Lower = [0, 0, 0];
% opts.StartPoint = [0.5, 0, 10];
% opts.Lower = [0, 0, -Inf];
%opts.Upper = [Inf, 0, Inf];

[fitresult, gof] = fit( xData, yData, ft, opts );

figure;
plot(fitresult);
hold on;
errorbar(omega_e, amps, amp_errs, amp_errs, '.');
title("Stable State Amplitude vs Excitatory Omega");
grid on;
xlabel('Excitatory Omega (1/s)');
ylabel('Stable State Amplitude (m)');
legend('Fit to $$ \frac{F}{\sqrt{(\omega_e^2-\omega_0^2)^2 + S\omega_e^2}} $$', "Amplitude " + text, 'Interpreter', 'latex');
set(gca, 'fontsize', 18);

[names, vals errs] = get_params_and_errors(fitresult);
F = vals(1); 
s = vals(2);
w0 = vals(3);
s_err = errs(2);
w0_err = errs(3);

% find width 
x = linspace(0, 40, 10000);
y = F./sqrt((x.^2-w0.^2).^2 + s*x.^2);
half_max = max(y) / 2;
goods = x(y>half_max); 
width = goods(end) - goods(1);
disp("width:" + width);

end
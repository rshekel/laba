function [omega, omega_err, amplitude, amplitude_err] = get_omega_and_amplitude(filepath)

% filepath = "..\Results\DrivenStableState\freq4.2_amp_0.5_0kg_stable\x0_0_v0_0_1.csv";
data = ImportCapstoneFile(filepath);

ends = 3;
times = data.Times(ends:end-ends);
positions = data.Positionm(ends:end-ends); 

time_interval = times(2) - times(1); % 0.025 

mean_position = mean(positions);

maxes = islocalmax(positions); 
max_indexes = find(maxes);
amplitude_maxes = positions(max_indexes); 

real_maxes = amplitude_maxes>mean_position;
amplitude_maxes = amplitude_maxes(real_maxes);
max_indexes = max_indexes(real_maxes);


differences = diff(max_indexes);
omegas = (2*pi) ./ (differences * time_interval);
% We assume (and see) that the frequency is constant up to resolution
% problems, so we simply take the mean. 
omega = mean(omegas);
omega_err = std(omegas); 

amplitude_max = mean(amplitude_maxes); 
amplitude_err = std(amplitude_maxes); 

base_height = mean(positions);
amplitude = amplitude_max - base_height;


%{
% Fitting doesn't work, and we get a bad amplitude, so we do the fit ourselves..

% Fit: 'dampening'.
[xData, yData] = prepareCurveData( times, positions );

% Set up fittype and options.
ft = fittype( 'a*cos(w*t+p)+ e', 'independent', 't', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [0.1 20 0.438744359656398 0.2];

% Fit model to data.
[fr, gof] = fit( xData, yData, ft, opts );

omega = fr.w;
confidence = confint(fr);
w_confidence = confidence(:, 5);  % 5 is the index of omega
omega_err = (w_confidence(2) - w_confidence(1)) / 2;

amplitude = fr.a;
confidence = confint(fr);
amplitude_confidence = confidence(:, 5);  % 5 is the index of omega
amplitude_err = (amplitude_confidence(2) - amplitude_confidence(1)) / 2;
%}

end

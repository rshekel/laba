function [dampening_factor, dampening_factor_err] = get_dampening_factor(path)
[fr, gof]=analyze_dampening(path);

dampening_factor = fr.b;
confidence = confint(fr);
w_confidence = confidence(:, 2);  % 2 is the index of b dampening factor
dampening_factor_err = (w_confidence(2) - w_confidence(1)) / 2;

end


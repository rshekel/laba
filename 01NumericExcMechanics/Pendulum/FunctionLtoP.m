function period = FunctionLtoP(l)
    g = 9.81;
    x = 24;
    theta_0 = pi/x;
    omega_0 = 0;
    y_vec = [theta_0; omega_0];

    dt = 0.01;
    final_t = 10;

    time_vec = dt:dt:final_t;
    theta_vec = zeros(length(time_vec));
    omega_vec = zeros(length(time_vec));

    for i = 1:length(time_vec)

        % define derivative function 
        % added sin() on -g/l even though it doesn't appear in exc. because it
        % makes more sense 
        f = @(y_vec) [y_vec(2);(-g/l)*sin(y_vec(1))];

        k1 = f(y_vec)*dt; 
        k2 = f(y_vec + k1/2)*dt; 
        k3 = f(y_vec + k2/2)*dt; 
        k4 = f(y_vec + k3)*dt; 

        next_y_vec = y_vec + (k1 + 2*k2 + 2*k3 + k4)/6;
        theta_vec(i) = next_y_vec(1);
        omega_vec(i) = next_y_vec(2);

        y_vec = next_y_vec; 
    end

    % calc rotation period 
    max_thetas = islocalmax(theta_vec); % [0, 0, ... 1[max], 0, 0]
    times_of_max = time_vec(max_thetas);
    period = times_of_max(2) - times_of_max(1);

end 
function error = calc_error(a, b)
    mean_value = (a + b) / 2;
    error = (a-b)/mean_value*100;
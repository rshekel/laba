g = 9.81;
l = 2;
x = 12;
theta_0 = pi/x;
omega_0 = pi/30;
dt = 0.01;
final_t = 100;

time_vec = dt:dt:final_t;
theta_vec = zeros(length(time_vec));
omega_vec = zeros(length(time_vec));

theta = theta_0;
omega = omega_0;
    
for i = 1:length(time_vec)
    % define derivatives 
    theta_dot = omega;
    omega_dot = -(g/l)*sin(theta);

    % calculate next step with euler 
    theta_vec(i) = theta + (theta_dot * dt);
    omega_vec(i) = omega + (omega_dot * dt);
    
    theta = theta_vec(i);
    omega = omega_vec(i);
end

% calc rotation period 
max_thetas = islocalmax(theta_vec); % [0, 0, ... 1[max], 0, 0]
times_of_max = time_vec(max_thetas);
period = times_of_max(2) - times_of_max(1);
expected_omega = sqrt(g/l);
expected_period = (2*pi)/expected_omega;

% plot graph
plot(time_vec, theta_vec);
title("Simple harmonic pendulum: dt=" + dt + "period=" + period + ". expected period:" + expected_period + ". theta_0=pi/" + x);
xlabel('time(s)')
ylabel('theta(rad)')

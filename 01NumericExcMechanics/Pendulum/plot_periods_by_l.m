ls = 0.2:0.2:5;
periods = zeros(1, length(ls));

for i = 1:length(ls)
    periods(i) = FunctionLtoP(ls(i));
end

plot(ls, periods);
title("Simple harmonic pendulum: period / l")
xlabel('l(m)')
ylabel('Period(s)')

% from here this is auto generated code from cftool
% Fit: 'fit to sqrt'.
[xData, yData] = prepareCurveData( ls, periods );

% Set up fittype and options.
ft = fittype( 'sqrt(a*x)', 'independent', 'x', 'dependent', 'y' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = 0.267702707072748;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.
figure( 'Name', 'fit to sqrt' );
h = plot( fitresult, xData, yData );
legend( h, 'periods vs. ls', 'fit to sqrt', 'Location', 'NorthEast' );
% Label axes
xlabel ls
ylabel periods
grid on

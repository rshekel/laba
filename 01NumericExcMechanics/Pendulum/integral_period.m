g = 9.81;
l = 2;
x = 3;
theta_0 = pi / x;
f = @(theta) 1./sqrt(cos(theta)-cos(theta_0));
integral_value = integral(f, 0, theta_0);
period_time = 4*sqrt(l/(2*g))*integral_value;

% Riemann sum:
dtheta = 0.00000001;
theta = 0:dtheta:theta_0;
numeric_integral_value = sum(f(theta) * dtheta);
period_time2 = 4*sqrt(l/(2*g))*numeric_integral_value;

disp("Period time with matlab integral: " + period_time);
disp("Period time with numeric integral: " + period_time2);
disp("Error: " + ((period_time2 - period_time) / period_time * 100) + "%");
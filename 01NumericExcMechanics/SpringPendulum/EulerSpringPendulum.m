function dX = EulerSpringPendulum(X, g, k, l,m, dt)
    dX = zeros(1, 4) ; 
    x = X(1); 
    y = X(2); 
    vx = X(3);
    vy = X(4);
    r = sqrt(x^2 + y^2); 
    dX(1) = x +  vx*dt; 
    dX(2) = y +  vy*dt;
    dX(3) = vx -  (k*x*(r - l) )/r/m *dt; 
    dX(4) = vy + (g - (k*y*(r - l) )/r/m)*dt; 
end 
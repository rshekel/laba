close all;

% Physical constants of system
g = 9.81 ;
k = 2;
l = 1;
m = 1; 

% Numerical constants 
dt = 0.04;
steps = 2000;

% Starting conditions 
x0 = x_eq;
y0 = y_eq;
vx0 = 0;
vy0 = 0;
X0 = [x0; y0; vx0; vy0];

X = SpringPendulumSimulator(X0, g, k, l, m, dt, steps);

x_vec = X(1, :);
y_vec = X(2, :);


% plot graph
time_vec = 0:dt:((steps -1) * dt);
hold on;
plot(time_vec, x_vec);
plot(time_vec, y_vec);

title("Spring pendulum");
xlabel('time(s)');
ylabel('x, y (m)');
legend("x(t)", "y(t)");

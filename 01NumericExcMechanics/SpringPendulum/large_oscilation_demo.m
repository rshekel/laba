close all;

% Physical constants of system
g = 9.81;
k = 2;
l = 1;
m = 1; 

% Numerical constants 
dt = 0.04;
steps = 500;

% Equilibrium:
x_eq = 0;
y_eq = l + m*g/k;

% Starting conditions 
x0 = x_eq + 80;
y0 = y_eq + 4;
vx0 = 0;
vy0 = 0;
X0 = [x0; y0; vx0; vy0];

X = SpringPendulumSimulator(X0, g, k, l, m, dt, steps);
x_vec = X(1, :);
y_vec = X(2, :);
animate_x_y(x_vec, y_vec, [-200, 200, -1, 10], "Spring Pendulum - large oscilation", true, 0.0001);

close all;

% Physical constants of system
g = 9.81 ;
k = 2;
l = 1;
m = 1; 

% Numerical constants 
dt = 0.04;
steps = 2000;

% Equilibrium:
x_eq = 0;
y_eq = l + m*g/k;

% Starting conditions 
x0 = x_eq;
y0 = y_eq;
vx0 = 0;
vy0 = 0;
X0 = [x0; y0; vx0; vy0];

X = SpringPendulumSimulator(X0, g, k, l, m, dt, steps);
x_vec = X(1, :);
y_vec = X(2, :);

% plot graph
time_vec = 0:dt:((steps -1) * dt);
hold on;
plot(time_vec, x_vec);
plot(time_vec, y_vec);
axis([0, 80, -2, 7]);

title("Spring pendulum - Equilibrium");
xlabel('time(s)');
ylabel('x, y (m)');
legend("x(t)", "y(t)");

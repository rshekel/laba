close all;

[x, y] = meshgrid(-10:0.01:10, -10:0.01:10);

% Physical constants of system
g = 9.81;
k = 2;
l = 1;
m = 1; 

calc_potential = @(x, y, m, g, k, l) (-m*g*y+1/2*k*(sqrt(x.^2+y.^2) - l).^2);
potential = calc_potential(x, y, m, g, k, l);
[min_val, min_index] = min(potential(:));

disp("Min potential energy: " + min_val + " at x: " + x(min_index) + " y: " + y(min_index));
mesh(x, y, potential);
title("Potential energy / (x,y)");
xlabel('x');
ylabel('y');

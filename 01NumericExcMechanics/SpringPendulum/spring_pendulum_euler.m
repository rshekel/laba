% Physical constants of system
g = 9.81 ;
k = 2;
l = 1;
m = 1; 

% Numerical constants 
dt = 0.04;
steps = 2000;

% Starting conditions 
x0 = 10;
y0 = 4;
vx0 = 0;
vy0 = 0;
X0 = [x0; y0; vx0; vy0];

% Start loop logic 
X = zeros(4, steps);
X(:, 1) = X0;

for i = 2:steps 
    X(:, i) = EulerSpringPendulum(X(:, i-1), g, k, l, m, dt);
end

x_vec = X(1, :);
y_vec = X(2, :);

% plot graph
% plot(x_vec, y_vec);
% 

% for i=1:steps
%     plot(x_vec(i), y_vec(i),'or','MarkerSize',5,'MarkerFaceColor','r')
%     title("Spring Pendulum movement. dt=" + dt + " Step: " + i);
%     % hold on
%     axis([-20 20 -20 20])
%     xlabel('x');
%     ylabel('y');
%     pause(.0001)
% 
% end
% 

% plot graph
time_vec = 0:dt:((steps -1) * dt);
hold on;
plot(time_vec, x_vec);
plot(time_vec, y_vec);

title("Spring pendulum (Euler): dt=" + dt);
xlabel('time(s)');
ylabel('x, y (m)');
legend("x(t)", "y(t)");
function X = SpringPendulumSimulator(X0, g, k, l, m, dt, steps)
% Start loop logic 
X = zeros(4, steps);
X(:, 1) = X0;

for i = 2:steps 
    X(:, i) = RungeKuttaSpringPendulum(X(:, i-1), g, k, l, m, dt);
end
close all;

% Physical constants of system
g = 9.81;
k = 2;
l = 1;
m = 1; 

% prepare dots of spring pendulum motion 
dt = 0.05;
steps = 3000;

x0 = 5;
y0 = 3;
vx0 = 1;
vy0 = 1;
X0 = [x0; y0; vx0; vy0];

X = SpringPendulumSimulator(X0, g, k, l, m, dt, steps);
x_vec = X(1, :);
y_vec = X(2, :);
calc_potential = @(x, y, m, g, k, l) (-m*g*y+1/2*k*(sqrt(x.^2+y.^2) - l).^2);
potential = calc_potential(x_vec, y_vec, m, g, k, l);

% potential / t graph 
figure; 
time = 0:dt:(steps-1)*dt;
plot(time, potential)
title("Potential energy / t");
xlabel('t(s)');
ylabel('Energy(J)');

% create potential graph
figure; 
[x, y] = meshgrid(-10:0.2:10, -30:0.2:15);
calc_potential = @(x, y, m, g, k, l) (-m*g*y+1/2*k*(sqrt(x.^2+y.^2) - l).^2);
potential = calc_potential(x, y, m, g, k, l);
surf(x, y, potential);
title("Potential energy / (x,y)");
xlabel('x');
ylabel('y');


hold on;
plot3(x_vec, y_vec, calc_potential(x_vec, y_vec, m, g, k, l), 'or','MarkerSize',1,'MarkerFaceColor','r');

% animation - works slow and weird... 
% for i=1:length(x_vec)
%     hold on;
%     pt = calc_potential(x_vec(i), y_vec(i), m, g, k, l);
%     plot3(x_vec(i), y_vec(i), pt, 'or', 'MarkerSize', 1, 'MarkerFaceColor','r');
%     pause(0.002);
% end

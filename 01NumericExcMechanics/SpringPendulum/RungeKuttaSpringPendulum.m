function dX = RungeKuttaSpringPendulum(X,  g1, k, R,m, dt)
    x = X(1); 
    y = X(2); 
    vx = X(3);
    vy = X(4); 
    f = @(x1,y1) -(k*x1*(sqrt(x1^2 + y1^2) - R ))/sqrt(x1^2 + y1^2)/m; 
    g = @(x1,y1) g1 - (k*y1*(sqrt(x1^2 + y1^2) - R ))/sqrt(x1^2 + y1^2)/m; 
    k1 = dt*[vx, vy, f(x, y), g(x,y)]'; 
    k2 = dt*[vx+k1(3)/2, vy+k1(4)/2, f(x+k1(1)/2, y+k1(2)/2), g(x+k1(1)/2, y+k1(2)/2)]';
    k3 = dt*[vx+k2(3)/2, vy+k2(4)/2, f(x+k2(1)/2, y+k2(2)/2), g(x+k2(1)/2, y+k2(2)/2)]';
    k4 = dt*[vx+k3(3), vy+k3(4), f(x+k3(1), y+k3(2)), g(x+k3(1), y+k3(2))]';
    dX = X +  1/6*(k1+ 2*k2 +2*k3 + k4 ); 
end 
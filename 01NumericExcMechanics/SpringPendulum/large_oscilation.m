close all;

% Physical constants of system
g = 9.81;
k = 2;
l = 1;
m = 1; 

% Numerical constants 
dt = 0.04;
steps = 500;

% Equilibrium:
x_eq = 0;
y_eq = l + m*g/k;

% Starting conditions 
x0 = x_eq + 80;
y0 = y_eq + 4;
vx0 = 0;
vy0 = 0;
X0 = [x0; y0; vx0; vy0];

X = SpringPendulumSimulator(X0, g, k, l, m, dt, steps);
x_vec = X(1, :);
y_vec = X(2, :);


% plot graph
figure;
plot(x_vec, y_vec);
title("Spring pendulum - large oscilation")
xlabel('x (m)');
ylabel('y (m)');

figure;
time_vec = 0:dt:((steps -1) * dt);
hold on;
plot(time_vec, x_vec);
plot(time_vec, y_vec);

title("Spring pendulum - large oscilation");
xlabel('time(s)');
ylabel('x, y (m)');
legend("x(t)", "y(t)");

% calc period
max_x = islocalmax(x_vec); % [0, 0, ... 1[max], 0, 0]
times_of_max_x = time_vec(max_x);
x_period = times_of_max_x(2) - times_of_max_x(1);

max_y = islocalmax(y_vec); % [0, 0, ... 1[max], 0, 0]
times_of_max_y = time_vec(max_y);
y_period = times_of_max_y(2) - times_of_max_y(1);

x_omega = 2*pi / x_period;
y_omega = 2*pi / y_period;

disp("x_omega=" + x_omega);
disp("y_omega=" + y_omega);
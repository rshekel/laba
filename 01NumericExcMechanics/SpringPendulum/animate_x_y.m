function animate_x_y(x_vec, y_vec, default_axis, graph_title, should_hold_on, pause_time)
for i=1:length(x_vec)
    plot(x_vec(i), y_vec(i),'or','MarkerSize',1,'MarkerFaceColor','r');
    title(graph_title);
    if should_hold_on
        hold on
    end
    axis(default_axis);
    xlabel('x');
    ylabel('y');
    pause(pause_time)
end

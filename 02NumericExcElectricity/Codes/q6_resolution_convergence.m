%% find U with different resolutions
ns = 2.^[4:8];
ns = [30, 40, 50, 60, 70, 80, 90, 100, 110, 150, 200]; 

U1s = zeros(1, length(ns));
U2s = zeros(1, length(ns));
for i = 1:length(ns)
    [U1, U2] = q6_get_energy(ns(i));
    U1s(i) = U1;
    U2s(i) = U2;
end

%% Plot
figure;
plot(ns, U1s, '*');
title("U vs resolution");
xlabel('resolution n');
ylabel('Energy'); 

hold on;
plot(ns, U2s, '*r');
legend("U1 by electric field", "U2 by potential and charge density");
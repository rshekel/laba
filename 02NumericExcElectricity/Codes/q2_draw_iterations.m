function ret_mat = q2_draw_iterations(mat, dx, dy, epsilon) 

n = size(mat); 
n_x = n(1);
n_y = n(2);

d_iter = 400;
iter_index = 0;

difference = epsilon + 1;
figure("Name", "Gradual iterations");

while difference > epsilon
    for iter=1:d_iter
        old_mat = mat; 
        for i = 2:n_x - 1 
            for j = 2:n_y - 1
                mat(i,j) = 1/2 * (1/(dx.^2 + dy.^2)) * (dy.^2*(old_mat(i+1,j) + old_mat(i-1,j)) + dx.^2*(old_mat(i,j+1) + old_mat(i,j-1)));
            end
        end
        difference = max(max(abs(old_mat-mat)));
        v_0 = 1;
        difference = difference / v_0;
        
        iter_index = iter_index+1;
        
    end
    
    clf();
    contourf(mat);
    colorbar;
    title("Iter index: " + iter_index);
    pause(0.001);
end
ret_mat = mat; 

end 
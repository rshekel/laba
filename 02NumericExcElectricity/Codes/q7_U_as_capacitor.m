%% find U with different M=Ly/Lx
M = [1, 3, 5, 7, 10, 12, 15, 18, 20, 30];

U1s = zeros(1, length(M));  
U2s = zeros(1, length(M));
for i = 1:length(M)
    [U1, U2] = q7_get_energy(0.01, 0.01*M(i), 50, 50*M(i));
    U1s(i) = U1;
    U2s(i) = U2;
end

%% Plot
figure;
subplot(2, 1, 1);
plot(M, U1s, '*');
title("U1 vs M=Ly/Lx");
xlabel('M=Ly/Lx');
ylabel('Energy'); 
hold on;
fplot(@calc_expected_U, xlim);

subplot(2, 1, 2);
plot(M, U2s, '*r');
title("U2 vs M=Ly/Lx");
xlabel('M=Ly/Lx');
ylabel('Energy'); 
hold on;
fplot(@calc_expected_U, xlim);

%legend("U1 by electric field", "U2 by potential and charge density");


%% Calc expected U:
function expected_U = calc_expected_U(m)
epsilon_0 = 8.854e-12;
original_V = 2;
V = original_V .* 0.8; % Because we cut 0.1CM from each end;

original_Lx = 0.01;
original_Ly = 0.01.*m;
Lx = original_Lx - 0.002;
Ly = original_Ly - 0.002;

% C should be epsilon_0*S/d. for dz, C is epsilon_0*ly/lx
C = epsilon_0 .* Ly ./ Lx;
expected_U = 1/2 .* C.* V^2;
end
epsilon = 0.00001;
n = 200;
dx = 0.01 / (n-1);
dy = 0.01 / (n-1);

[Ex, Ey, sol] = q3_get_electric_field(epsilon, n);
% [Ex, Ey, sol] = q8_get_electric_field(epsilon, 0.01, 0.01, n, n);

% Make dimensions agree 
Ex = Ex(1:end-1, :); 
Ey = Ey(:, 1:end-1); 
sol = sol(1:end-1, 1:end-1); 

% Remove edges with diverging quantity 
margin = 10;
Ex = Ex(margin:end-margin, margin:end-margin); 
Ey = Ey(margin:end-margin, margin:end-margin); 

% resizing scale to get a seeable amount of arrows 
resize_scale = 1./10;
Ex = imresize(Ex, resize_scale);
Ey = imresize(Ey, resize_scale);
[rows, cols] = size(Ex);

[x, y] = meshgrid(linspace(margin*dx, 0.01-(margin+1).*dx, rows), linspace(margin*dy, 0.01-(margin+1).*dy, cols));


% sol = imresize(sol, resize_scale);
[xx, yy] = meshgrid(dx:dx:0.01, dy:dy:0.01);
figure;
contour(xx, yy, sol);
colorbar;
hold on;
quiver(x, y, Ex, Ey);
title("Electric Field in XY Plane with n = " + n);

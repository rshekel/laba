function [Ex, Ey, sol] = q8_get_electric_field(epsilon, Lx, Ly, nx, ny)
dx = Lx / (nx-1);
dy = Ly / (ny-1);

mat = q2_create_initial_potential(nx, ny);
sol = q8_solve_potential(mat, dx, dy, epsilon); 

% Calc Field 
Ex = -diff(sol, 1, 2)./dx;
Ey = -diff(sol, 1, 1)./dy;

% Make dimensions agree 
Ex = Ex(1:end-1, :);
Ey = Ey(:, 1:end-1);
sol = sol(1:end-1, 1:end-1);
end


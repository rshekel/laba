epsilon_0 = 8.854e-12;
epsilon = 0.00001;
n = 200;

[Ex, ~, ~] = q3_get_electric_field(epsilon, n);
E_side = Ex(:, 1);
sigma = E_side .* epsilon_0; 

figure; 
x = linspace(0, 1, length(E_side));
plot(x, sigma); 
title("Surface density at (0,y)"); 
xlabel("y (cm)");
ylabel('\sigma(y)'); 

function mat = q2_create_initial_potential(nx, ny, potential_type)
if nargin == 2
    potential_type = 4;
end

mat = zeros(ny, nx);
v_0 = 1;    

% initial state 
if potential_type == 1
    % Given initial potential
    mat(:, 1) = v_0;
    mat(:, end) = -v_0;

% Linear potential, like a plate capacitor
elseif potential_type == 2
    mat(:, 1) = v_0;
    mat(:, end) = -v_0;
    mat(1,:) = linspace(1, -1, nx);
    mat(end,:) = linspace(1, -1, nx);

% Equal on borders, like a conductor.
elseif potential_type == 3
    mat(:, 1) = v_0;
    mat(:, end) = v_0;
    mat(1,:) = v_0;
    mat(end,:) = v_0;
    %mat(1,1) = 0;  % Just to keep a reference point

elseif potential_type == 4
    mat(:, 1) = v_0;
    mat(:, end) = -v_0;
    mat(1,:) = sin(linspace(0, 2*pi, nx));
    mat(end,:) = sin(linspace(0, 2*pi, nx));
end


end

function [Ex, Ey, sol] = q3_get_electric_field(epsilon, n)
%{
nx = n;
ny = n;
dx = 0.01 / (nx-1);
dy = 0.01 / (ny-1);

mat = q2_create_initial_potential(nx, ny);
sol = q2_solve_potential(mat, dx, dy, epsilon); 

% Calc Field 
Ex = -diff(sol, 1, 2)./dx;
Ey = -diff(sol, 1, 1)./dy;
%}
[Ex, Ey, sol] = q7_get_electric_field(epsilon, 0.01, 0.01, n, n);
end


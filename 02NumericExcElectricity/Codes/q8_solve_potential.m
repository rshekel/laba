%% Solve potential with cyclic bounds
function ret_mat = q8_solve_potential(mat, dx, dy, epsilon) 
[n_x, n_y] = size(mat);
difference = epsilon + 1;

while difference > epsilon 
    old_mat = mat;
    for i = 2:n_x-1
        for j = 1:n_y
            x_1 = i-1;
            x_2 = i + 1;
            y_1 = mod(j-2,n_y) + 1;
            y_2 = mod(j,n_y) + 1;
            mat(j, i) = 1/2 * (1/(dx.^2 + dy.^2)) * (dy.^2*(old_mat(j, x_2) + old_mat(j, x_1)) + dx.^2*(old_mat(y_2, i) + old_mat(y_1, i)));
        end
    end
    difference = max(max(abs(old_mat-mat)));
    v_0 = 1;
    difference = difference / v_0;
end

ret_mat = mat; 

end 
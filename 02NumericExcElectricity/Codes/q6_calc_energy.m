function [U1, U2] = q6_calc_energy(Ex, Ey, potential, dx, dy)
epsilon_0 = 8.854e-12;
%% Calc energy from field 
E = sqrt(Ex.^2 + Ey.^2);
U1 = (epsilon_0/2) * sum(sum(E.^2)) .* dx .* dy; 
% disp("U_2: " + U2); 


%% Calc energy from potential and surface density 
E_side = Ex(:, 1);
sigma = E_side .* epsilon_0;
side_a_U = sum(sigma.*potential(1:end, 1).*dy);

E_side = Ex(:, end);
sigma = -E_side .* epsilon_0;
side_b_U = sum(sigma.*potential(1:end, end).*dy);


E_side = Ey(1, :);
sigma = E_side .* epsilon_0;
side_c_U = sum(sigma.*potential(1, :).*dx);

E_side = Ey(end, :);
sigma = -E_side .* epsilon_0;
side_d_U = sum(sigma.*potential(end, :).*dx);

U2 = 1/2*(side_a_U + side_b_U + side_c_U + side_d_U);

end
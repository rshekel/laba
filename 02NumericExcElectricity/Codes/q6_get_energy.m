function [U1, U2] = q6_get_energy(n)
epsilon = 0.000001;
dx = 0.01 / (n-1);
dy = 0.01 / (n-1);

[Ex, Ey, potential] = q3_get_electric_field(epsilon, n);

%% Make dimensions agree 
Ex = Ex(1:end-1, :);
Ey = Ey(:, 1:end-1);

% Remove edges with diverging quantity 
margin = floor(n/10);
Ex1 = Ex((margin+1):end-margin, (margin+1):end-margin); 
Ey1 = Ey((margin+1):end-margin, (margin+1):end-margin); 
potential1 = potential((margin+1):end-margin, (margin+1):end-margin);
potential1 = potential1(1:end-1, 1:end-1);

[U1, U2] = q6_calc_energy(Ex1, Ey1, potential1, dx, dy);
end
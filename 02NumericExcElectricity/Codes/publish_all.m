output_path = "Output";
[~, ~, ~] = mkdir(output_path);

project_name = "Numeric Exc 77102";
TEMP_SCRIPT_NAME = "temp_publish_all_script.m";

fid = fopen(TEMP_SCRIPT_NAME,'w');
fprintf(fid, "%%%% %s\n", project_name);

files = dir();

for i=1:length(files)
    name = files(i).name;
    if name == "." || name == ".."
        continue
    elseif name == "publish_all.m"
        continue
    elseif name == TEMP_SCRIPT_NAME
        continue
    end
    if files(i).isdir
        continue
    end
    matched = regexp(name, '.*\.m$', 'match', 'once');
    if isempty(matched)
        disp("Skipping " + name);
        continue;
    end
    
    fprintf(fid, "%%%% %s\n", name);
    fprintf(fid, "%%\n");
    fprintf(fid, "%% <include>%s</include>\n", name);
    fprintf(fid, "%%\n");
end
fclose(fid);
publish(TEMP_SCRIPT_NAME, "outputDir", "Output", "format", "pdf", "evalCode", false);
[~, script_name, ~] = fileparts(TEMP_SCRIPT_NAME); % Get file name without .m suffix
delete(TEMP_SCRIPT_NAME);
movefile(fullfile("Output", script_name + ".pdf"), fullfile("Output", project_name + "Codes.pdf"));
function ret_mat = q2_solve_potential(mat, dx, dy, epsilon) 

n = size(mat); 
n_x = n(1);
n_y = n(2);

difference = epsilon + 1;

diffs = []; 

while difference > epsilon 
    old_mat = mat; 
    for i = 2:n_x - 1 
        for j = 2:n_y - 1
            mat(i,j) = 1/2 * (1/(dx.^2 + dy.^2)) * (dy.^2*(old_mat(i+1,j) + old_mat(i-1,j)) + dx.^2*(old_mat(i,j+1) + old_mat(i,j-1)));
        end
    end
    difference = max(max(abs(old_mat-mat)));
    diffs = [diffs, difference];
    v_0 = 1;
    difference = difference / v_0;
end

% This is for showing convergence of itterations 
%{ 
figure; 
plot(2:100, diffs(2:100)); 
xlabel('Iteration index');
ylabel('difference from last iteration');
figure; 
plot(100:length(diffs), diffs(100:end)); 
xlabel('Iteration index');
ylabel('difference from last iteration');
%}
ret_mat = mat; 

end 
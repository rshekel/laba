function [U1, U2] = q7_get_energy(Lx, Ly, nx, ny)
epsilon = 0.00001;
dx = Lx / (nx-1);
dy = Ly / (ny-1);

[Ex, Ey, potential] = q7_get_electric_field(epsilon, Lx, Ly, nx, ny);

%% Make dimensions agree 
Ex1 = Ex(1:end-1, :);
Ey1 = Ey(:, 1:end-1);

% Remove edges with diverging quantity 
xmargin = floor(nx.*(0.001/Lx));
ymargin = floor(ny.*(0.001/Ly));

Ex1 = Ex1((ymargin+1):end-ymargin, (xmargin+1):end-xmargin); 
Ey1 = Ey1((ymargin+1):end-ymargin, (xmargin+1):end-xmargin); 
potential1 = potential((ymargin+1):end-ymargin, (xmargin+1):end-xmargin);
potential1 = potential1(1:end-1, 1:end-1);

[U1, U2] = q6_calc_energy(Ex1, Ey1, potential1, dx, dy);

%{
%% Plot
% resizing scale to get a seeable amount of arrows 
resize_scale = 1./10;
Exx = imresize(Ex, resize_scale);
Eyy = imresize(Ey, resize_scale);
[rows, cols] = size(Exx);

[x, y] = meshgrid(linspace(xmargin*dx, Lx-(xmargin+1).*dx, cols), linspace(ymargin*dy, Ly-(ymargin+1).*dy, rows));


[rows, cols] = size(potential1);
[xx, yy] = meshgrid(linspace(xmargin*dx, Lx-(xmargin+1).*dx, cols), linspace(ymargin*dy, Ly-(ymargin+1).*dy, rows));
figure;
contour(xx, yy, potential1);
colorbar;
hold on;
quiver(x, y, Exx, Eyy);
title("Electric Field in XY Plane with M =" + Ly/Lx);
%}
end
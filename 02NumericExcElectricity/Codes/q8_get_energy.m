function [U1, U2] = q8_get_energy(Lx, Ly, nx, ny)
epsilon = 0.0000001;
dx = Lx / (nx-1);
dy = Ly / (ny-1);

[Ex, Ey, potential] = q8_get_electric_field(epsilon, Lx, Ly, nx, ny);

% Remove edges with diverging quantity 
xmargin = 5;
ymargin = 5;

Ex = Ex((ymargin+1):end-ymargin, (xmargin+1):end-xmargin); 
Ey = Ey((ymargin+1):end-ymargin, (xmargin+1):end-xmargin); 
potential = potential((ymargin+1):end-ymargin, (xmargin+1):end-xmargin);

[U1, U2] = q6_calc_energy(Ex, Ey, potential, dx, dy);
end
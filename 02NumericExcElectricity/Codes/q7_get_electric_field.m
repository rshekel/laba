function [Ex, Ey, sol] = q7_get_electric_field(epsilon, Lx, Ly, nx, ny)
dx = Lx / (nx-1);
dy = Ly / (ny-1);

mat = q2_create_initial_potential(nx, ny);
sol = q2_solve_potential(mat, dx, dy, epsilon); 

% Calc Field 
Ex = -diff(sol, 1, 2)./dx;
Ey = -diff(sol, 1, 1)./dy;
end


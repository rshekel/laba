%% find U with different n
ns = [30, 40, 50, 60, 70, 80, 90, 100, 110, 150, 200]; 

U1s = zeros(1, length(ns));
U2s = zeros(1, length(ns));
for i = 1:length(ns)
    [U1, U2] = q8_get_energy(0.01, 0.01, ns(i), ns(i));
    U1s(i) = U1;
    U2s(i) = U2;
end

expected_U = calc_expected_U();

%% Plot
figure;
plot(ns, U1s, '*');
title("U vs resolution");
xlabel('resolution n');
ylabel('Energy'); 
hold on;
plot(ns, U2s, '*r');
plot(xlim, [expected_U, expected_U]);

legend("U1 by electric field", "U2 by potential and charge density", "Expected energy");


%% Calc expected U:
function expected_U = calc_expected_U()
epsilon_0 = 8.854e-12;
V = 2;

Lx = 0.01;
Ly = 0.01;

% C should be epsilon_0*S/d. for dz, C is epsilon_0*ly/lx
C = epsilon_0 .* Ly ./ Lx;
expected_U = 1/2 .* C.* V^2;
end
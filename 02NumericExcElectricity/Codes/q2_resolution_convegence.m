ns = 2.^(1:8);

epsilon = 0.00001;
mat_mean = @(m) mean(m(:));

means = [];

for n = ns
    nx = n;
    ny = n;
    dx = 0.01 / nx;
    dy = 0.01 / ny;

    mat = q2_create_initial_potential(nx, ny);
    sol = q2_solve_potential(mat, dx, dy, epsilon); 
    
    means = [means, mat_mean(abs(sol))];

end

plot(ns, means, '*');
xlabel('resolution n');
ylabel('mean of solution (g)'); 

% will draw last (and best solution)
figure; 
contourf(sol);
colorbar;
